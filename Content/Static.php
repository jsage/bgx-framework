<?php
/**
 * @package BgxCms
 * @author Jens Sage <j.sage@bgx-mail.de>
 */
class Bgx_Content_Static
{
    /**
     * @param  array $cols
     * @param  string $site
     * @return array
     */
    private static function _site($cols, $site)
    {
        $cache_id = 's_content_' . $site . '_' . Bgx_Core::locale()->getLanguage();
        if (!$data = Bgx_Core::cache()->load($cache_id))
        {
            $sel = Bgx_Core::database()->select()->from('sitecontent',
                                                        array('title', 'content'));
    	    $sel->joinInner('sites',
                            Bgx_Core::database()->quoteIdentifier('sitecontent') . '.' .
    	                    Bgx_Core::database()->quoteIdentifier('site_id') . ' = ' .
    	                    Bgx_Core::database()->quoteIdentifier('sites') . '.' .
    	                    Bgx_Core::database()->quoteIdentifier('id'),
    	                    array('name'));
    	    $sel->where(Bgx_Core::database()->quoteIdentifier('sitecontent') . '.' .
    	                Bgx_Core::database()->quoteIdentifier('language_id') . ' = ?',
    	                Bgx_Core::translate()->getNumByIdentifier(Bgx_Core::locale()->getLanguage()),
    	                Zend_Db::INT_TYPE);
    	    try
    	    {
    	        $result = Bgx_Core::database()->query($sel);
    	        $data = array();
    	        while ($col = $result->fetch())
    	        {
    	            $data[$col['name']] = array('title'   => $col['title'],
    	                                        'content' => $col['content']);
    	        }
    	        Bgx_Core::cache()->save($data, $cache_id);
    	    }
    	    catch (Exception $e)
    	    {
    	        Bgx_Core::log()->log('Bgx_StaticContent: Can\'t fetch static content: "' .
    	                             $e->getMessage() . '". SQL: ' . $sel->__toString(), Zend_Log::CRIT);
    	        return false;
    	    }
        }
        foreach ($cols AS $col)
        {
            $return[$col] = isSet($data[$site][$col]) ? $data[$site][$col] : null;
        }
        return $return;
    }

    /**
     * @param  string $name
     * @return string
     */
    public static function getSite($name)
    {
        return self::_site(array('title', 'content'), $name);
    }

    /**
     * @param string $name
     * @return string
     */
    public static function getTitle($name)
    {
        if ($tmp = self::_site(array('title'), $name))
        {
            return $tmp['title'];
        }
        else
        {
            return false;
        }
    }

    /**
     * @param  string $name
     * @return string content
     */
    public static function getContent($name)
    {
        if ($tmp = self::_site(array('content'), $name))
        {
            return $tmp['content'];
        }
        else
        {
            return false;
        }
    }
}