<?php
/**
 * Bgx_Observer
 * @package	BgxCms
 * @author	Jens Sage <j.sage@bgx-mail.de>
 */
abstract class Bgx_Event_Observer
{
	/**
	 * @var Bgx_Event_Subject
	 */
	private $subject = null;

	public final function attach(Bgx_Event_Subject $subject)
	{
		$this->subject = $subject;
		$this->subject->attach($this);
	}

	public final function detach()
	{
		if ($this->subject !== null)
		{
			$this->subject->detach($this);
		}
	}

	public abstract function update();

	/**
	 * @return Bgx_Event_Subject
	 */
	public final function getSubject()
	{
		return $this->subject;
	}
}