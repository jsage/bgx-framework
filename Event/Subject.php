<?php
/**
 * @package	BgxCms
 * @author	Jens Sage <j.sage@bgx-mail.de>
 */
abstract class Bgx_Event_Subject {
	private $observers = array();

	public final function attach(Bgx_Event_Observer $observer)
	{
		$this->observers[] = $observer;
	}

	public final function detach(Bgx_Event_Observer $observer)
	{
		for ($i = 0; $i < sizeof($this->observers); $i++)
		{
			if ($this->observers[$i] === $observer)
			{
				unset($this->observers[$i]);
			}
		}
	}

	public final function fire()
	{
		for ($i = 0; $i < sizeof($this->observers); $i++)
		{
			$this->observers[$i]->update();
		}
	}

	public abstract function getState();
}