<?php
/**
 * Abstraction of InstantMessaging
 * 
 * @author  Jens Sage <j.sage@bgx-mail.de>
 * @package BgxCore
 */
abstract class Bgx_IM_Abstract
{
	protected $socket = NULL;
	
	/**
	 * options can contain host, user, password, resource, encryption, trallala...
	 * 
	 * it's recommended to not connect to servers at construct, because of lazy connections.
	 * 
	 * @param string $service the class name of 
	 * @param array  $options
	 */
	public abstract function __construct(array $options);
	
	/**
	 * connect to server.
	 * 
	 * @throws Exception
	 * @return void
	 */
	private abstract function connect();
	
	/**
	 * disconnects...
	 */
	private abstract function disconnect();
	
	/**
	 * @see Bgx_IM_Abstract
	 */
	public function __destruct()
	{
		if (!is_null($this->socket))
		{
			$this->disconnect();
		}
	}
	
	/**
	 * returns a brandnew service-object of type $service (=classname).
	 *
	 * @param  string $service classname. e.g.: Bgx_IM_Jabber
	 * @param  array  $options
	 * @return Bgx_IM_Abstract
	 */
	public static function getService($service, array $options)
	{
		if (!class_exists($service, true))
		{
			return false;
		}
		return new $service(&$options);
	}
}