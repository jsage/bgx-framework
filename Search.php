<?php
/**
 * @author  Jens Sage <j.sage@babbls.de>
 * @package BgxCms
 */
class Bgx_Search
{
	/**
	 * @param	string						$searchterm
	 * @param	Bgx_Search_Engine_Abstract	$engine
	 * @param	array						$options
	 * @return	Bgx_Search_Engine_Abstract
	 */
	public static function search($searchterm = "", Bgx_Search_Engine_Abstract $engine, array $options = array())
	{
		$searchterm = trim($searchterm);
		if (strlen($searchterm) < 3)
		{
			return false;
		}

		$engine->setOptions($options);

		return $engine->search($searchterm);
	}

	/**
	 * @param	string	$searchterm
	 * @param	array	$engines
	 * @param	array	$options
	 * @return	array
	 */
	public static function multipleSearch($searchterm = "", array $engines, array $options = array())
	{
		if (!count($engines))
		{
			throw new Exception('You should use at least one search-engine!');
		}

		$result = array();

		foreach ($engines AS $engine)
		{
			$result[get_class($engine)] = self::search($searchterm, $engine, $options);
		}

		return $result;
	}
}