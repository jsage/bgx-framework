<?php 

class Bgx_User
{
	private	$id, 
			$username,
			$password,
			$email,
			$active,
			$language_id,
			$incomplete = true;
	
	public function usernameExists($username)
	{
		$username = Bgx_Core::getDb()->quote($username, PDO::PARAM_STR);
		
		return (bool) array_value(Bgx_Core::getDb()->query(
			"SELECT COUNT(id) AS n FROM users WHERE username = {$username}"
		)->fetch(PDO::FETCH_ASSOC), 'n');
	}
			
	public function __construct(array $data)
	{
		if (!isset($data['id']))
			throw new Exception("User ID must be given.");
		$this->id = $data['id'];
		$n = 5;
		foreach ($data AS $key => $value)
		{
			switch ($key)
			{
				case 'username':
					$this->username = $value;
					$n--;
					break;
				case 'password':
					$this->password = $value;
					$n--;
					break;
				case 'email':
					$this->email = $value;
					$n--;
					break;
				case 'active':
					$this->active = $value;
					$n--;
					break;
				case 'language_id':
					$this->language_id = $value;
					$n--;
					break;
				default:
					continue;
			}
		}
		$if ($n == 0)
			$this->incomplete = false;
	}
	
	public function getId()
	{
		return (int) $this->id;
	}
	
	public function getName()
	{
		
	}
	
	/**
	 * @throws	PDOException
	 * @param	string	$username
	 * @return	bool
	 */
	public function setName($username)
	{
		$username = Bgx_Core::getDb()->quote($username, PDO::PARAM_STR);
		if (!$this->usernameExists())
		{
			return Bgx_Core::getDb()->query("UPDATE users SET username = {$username}");
		}
		return false;
	}
	
	public function getGroups()
	{
		$res = Bgx_Core::getDb()->query(
			"SELECT group_id AS id FROM users_to_groups WHERE user_id = " . $this->getId()
		)->fetchAll(PDO::FETCH_ASSOC);
		
		$groups = array();
		
		while ($group = $res->fetch())
		{
			
		}
		
		return $groups;
	}
	
	public function isAuthorisedTo($action)
	{
		
	}
}
