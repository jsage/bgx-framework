<?php
/**
 * @author  Jens Sage <j.sage@bgx-mail.de>
 * @package BgxCms
 */
class Bgx_Install
{
	/**
	 * @var Bgx_Install
	 */
    private static $instance    = NULL;

    /**
     * singleton...
     */
    public static function run()
    {
        if (is_null(self::$instance))
        {
            self::$instance = new self;
        }
    }

    /**
     *
     */
    private function __construct()
    {
    	// Throw the Main-Template
    	if (!count(Bgx_Core::mvc()->getPath()))
    	{
    		Bgx_Core::template('Main', null, LIBRARY . '/Bgx/Install/Templates/', false);

     	}
    }

    /**
     * singleton...
     */
    private function __clone() {}



    public function installFilesInto()
    {
        touch($this->tmpConf['appdir'] . '.htaccess');
    }

    /**
     * @return bool
     */
    private function createAdminUser($username, $password, $email)
    {
        $this->dbConn()->insert('users',
                                array('username' => $username,
                                      'password' => md5($password),
                                      'email'    => $email,
                                      'active'   => Zend_Db_Expr('true'),
                                      'reg_date' => new Zend_Db_Expr('CURDATE()')));
        $this->dbConn()->insert('users_to_groups',
                                array('user_id'  => $this->dbConn()->lastInsertId(),
                                      'group_id' => 4));
    }
}
 
/**
 * If this file is includet, it will start the install on Bgx_Core::__construct()
 *
 */
class StartInstall implements SyndiCat_Observer_Subject
{
    public function Notify($e, Array $data)
    {
        Bgx_Install::run();
    }
}

SyndiCat_Observer::GetInstance()->AddSleepSubject('Bgx_Core::__construct', 'StartInstall');