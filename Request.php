<?php
/**
 * @author Jens Sage <j.sage@bgx-mail.de>
 */
class Bgx_Request
{
	/**
	 * @var Bgx_Request
	 */
	private static $_instanceObj;
	
	/**
	 * @var string
	 */
	private $_base_path = "";
	
	private $_protocol = 'http';
	
	/**
	 * @var array
	 */
	private $_params = array();
	
	/**
	 * @return void
	 */
	private function __construct()
	{		
		$this->detectBasePath();
		$this->detectParams();
	}
	
	/**
	 * @return void
	 */
	private function __clone() {}
	
	/**
	 * @return Bgx_Request
	 */
	public static function getInstance()
	{
		if (self::$_instanceObj === null)
		{
			self::$_instanceObj = new self;
		}
		return self::$_instanceObj;
	}
	
	/**
	 * @return void
	 */
	private function detectBasePath()
	{
		$filename = basename($_SERVER['SCRIPT_FILENAME']);
		if (basename($_SERVER['SCRIPT_NAME']) === $filename)
		{
			$this->_base_path = $_SERVER['SCRIPT_NAME'];
		}
		else if (basename($_SERVER['PHP_SELF']) === $filename)
		{
			$this->_base_path = $_SERVER['PHP_SELF'];
		}
		else
		{
			$path    = $_SERVER['PHP_SELF'];
			$segs    = explode('/', trim($_SERVER['SCRIPT_FILENAME'], '/'));
			$segs    = array_reverse($segs);
			$index   = 0;
			$last    = count($segs);
			do
			{
				$seg     = $segs[$index];
				$this->_base_path = '/' . $seg . $baseUrl;
				++$index;
			} while (($last > $index) && (false !== ($pos = strpos($path, $this->_base_path))) && (0 != $pos));
		}
		$this->_base_path = urldecode(str_replace('/index.php', '', $this->_base_path));
	}
	
	/**
	 * @return void
	 */
	private function detectParams()
	{
		$path = str_replace(
			$this->_base_path,
			"",
			str_replace(
				'index.php',
				"",
				isSet($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['SCRIPT_NAME']
			)
		);
		
		if ($pos = strpos($path, '?'))
			$path = substr($path, 0, $pos);
		
		$path = substr($path, 1);
		
		$this->_params = explode('/', $path);
	}
	
	/**
	 * @param	$index
	 * @param	$value
	 * @return	Bgx_Request
	 */
	public function setVarAtIndex($index, $value)
	{
		$this->_params[$index] = $value;
		return $this;
	}
	
	/**
	 * @param	int			$index
	 * @return	string|null
	 */
	public function getVarAtIndex($index)
	{
		return isset($this->_params[$index]) ? $this->_params[$index] : null;
	}
	
	/**
	 * @param	string		$index
	 * @return	int
	 */
	public function getIndex($var)
	{
		return array_search($var, $this->_params);
	}
	
	/**
	 * We take an var as a key
	 * @param	string	$var
	 * @return	string|null
	 */
	public function getValue($var)
	{
		$index = $this->getIndex($var);
		if ($index !== false)
		{
			return $this->getVarAtIndex($index + 1);
		}
		else
		{
			return null;
		}
	}
	
	/**
	 * @return bool
	 */
	public function isGet()
	{
		return $_SERVER['REQUEST_METHOD'] == 'GET';
	}

	/**
	 * @return bool
	 */
	public function isPost()
	{
		return $_SERVER['REQUEST_METHOD'] == 'POST';
	}

	/**
	 * @return	bool
	 */
	public function isXmlHttpRequest()
	{
		return (
			isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
			strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
		);
	}
	
	/**
	 * @return string
	 */
	public function getUrlString()
	{
		return $this->_protocol . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}
	
	/**
	 * @return string
	 */
	public function getBaseUrlString()
	{
		return $this->_protocol . '://' . $_SERVER['HTTP_HOST'] . $this->_base_path;
	}
}