<?php

final class Bgx_Image_Svg extends Bgx_Image_Abstract
{
    private $_definitions = array();
    private $_g           = array();
    private $_buffer      = "";
    /**
     * in px
     *
     * @var array
     */
    private $dimensions   = array('x' => 0, 'y' => 0);
    
    private $_generated   = false;
    private $_svg_version = 1.0;
    
    public function __construct($width, $height)
    {
        $this->dimensions['y'] = (strpos($height, 'px') !== false) ? $height : 0;
        $this->dimensions['x'] = (strpos($width, 'px') !== false) ? $width : 0;
        
        $this->_buffer .= '<?xml version="' . $this->_svg_version . '" encoding="utf-8" standalone="yes"?>' . "\n" .
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">' . "\n" .
        '<svg xmlns="http://www.w3.org/2000/svg" height="' . $this->dimensions['y'] . 
        'px" width="' . $this->dimensions['x'] . 'px" xmlns:xlink="http://www.w3.org/1999/xlink">' . "\n";
    }
    
    public function getXml()
    {
        if (!$this->_generated)
        {
            $this->_buffer .= '<defs>';
            $this->_buffer .= '</defs>';
            
            $this->_buffer .= '</svg>';
            $this->_generated = true;
        }
        return $this->_buffer;
    }
    
    public function getHttpHeader()
    {
        return 'image/svg'; // FIXME correct this
    }
}