<?php
/**
 * @author  Jens Sage <j.sage@bgx-mail.de>
 * @package BgxCms
 */
abstract class Bgx_Image_Abstract
{
	/**
	 * @param int $width
	 * @param int $height
	 */
	public abstract function __construct($width, $height);

	/**
	 * Enter description here...
	 *
	 */
	public abstract function save();

	/**
	 * Enter description here...
	 *
	 * @param unknown_type $filename
	 * @param unknown_type $force_type
	 * @param unknown_type $quality
	 */
	public abstract function saveAs($filename, $force_type, $quality);
}