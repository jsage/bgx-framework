<?php
/**
 * @author  Jens Sage <j.sage@bgx-mail.de>
 * @package BgxCms
 */
class Bgx_Image_Common
{
    /**
     * @param  string $file path to file
     * @return array        array('width' => 0, 'height' => 0);
     */
	public static function imageSize($file)
	{

	}

	/**
	 * @param  string $suffix
	 * @return string
	 */
	public static function mimeTypeBySuffix($suffix)
	{

	}

	/**
	 * @return array
	 */
	public static function mimeTypeList()
	{

	}

	/**
	 * @param  string $type
	 * @return string
	 */
	public static function suffixByMimeType($type)
	{

	}

	/**
	 * @param  array  $rgb  array('r' => 0, 'g' => 0, 'b' => 0)
	 * @param  bool   $hex3 hex3-output?
	 * @return string       #000000
	 */
	public static function hexByRgb($rgb, $hex3 = false)
	{

	}

	/**
	 * @param  string $hex
	 * @return array  $rgb  array('r' => 0, 'g' => 0, 'b' => 0)
	 */
	public static function rgbByHey($hex)
	{

	}

	/**
	 * @param string $hex6
	 */
	public static function toHex3($hex6)
	{

	}
}