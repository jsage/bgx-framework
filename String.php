<?php
/**
 * String-class
 * @author  Jens Sage <j.sage@bgx-mail.de>
 * @package BgxCms
 */

/**
 * 
 * OBSOLETE ... THROW IT AWAY
 *
 */
class Bgx_String
{
	private $string = '';

	public function __construct($string = "")
	{
		$this->string =& $string;
	}

    /**
     * @return bool
     */
    public function containsBadWords()
    {
        foreach ($this->badwords AS $word)
        {
            if (stripos($this->string, $word) !== NULL)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * @param  string $replacement
     * @return Bgx_BadwordFilter
     */
    public function replaceBadWords($replacement = '*')
    {
        $this->string = str_ireplace($this->badwords, str_repeat($replacement, @strlen($this->badwords)), $this->string);

        return $this;
    }

    /**
     * @param array     $search     what do you search for?
     * @param highlight $highlight  in sprintf-syntax (%s = mark)
     */
    public function mark(array $search, $highlight = '<span class="marked">%s</span>')
    {
        foreach ($search AS $term)
        {

        }
    }

    /**
     * wraps long lines/words in a text
     *
     * @param int    $width max length per line
     * @param string $break wrap?
     * @param bool   $cut   cut even words?
     */
    public function wrap($width = 20, $break = "\n", $cut = true)
    {
    	$this->string = wordwrap($this->string, &$width, &$break, $cut);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->string;
    }
}