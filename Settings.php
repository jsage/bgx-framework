<?php

class Bgx_Settings
{
    private $data = array();
    private static $instance = NULL;
    private $cache = false;

    public static $types = array('boolean',
                                 'integer',
                                 'float',
                                 'string',
                                 'array',
                                 'null');

    private function _load()
    {
        $q = Bgx_Core::database()->select()->from('settings', array('var', 'value', 'type'));
        $q = Bgx_Core::database()->query($q);
        while ($row = $q->fetch())
        {
            $this->data[$row['var']] = $row['value'];
            settype($this->data[$row['var']], $row['type']);

            Bgx_Core::register($row['var'], $row['value']);
        }
    }

    private function __construct()
    {
        if (!$this->data = Bgx_Core::cache()->load('settings'))
        {
            $this->_load();
            $this->cache = true;
        }
    }

    function __destruct()
    {
        if ($this->cache)
        {
            Bgx_Core::cache()->save($this->data, 'settings');
        }
    }

    /**
     * @return Bgx_Settings
     */
    public static function getInstance() {
        return (self::$instance === NULL) ?  self::$instance = new self : self::$instance;
    }

    private function __clone() {}

    public function __get($id)
    {
        if (isSet($this->data[$id]))
        {
            return $this->data[$id];
        }
    }

    public function __set($id, $value)
    {
        Bgx_Core::database()->update('settings',
                                     array('value' => $value),
                                     Bgx_Core::database()->quoteIdentifier('var') . ' = ' .
                                     Bgx_Core::database()->quote($key));
        $his->data[$id] = $value;
    }

    public function addSetting($id, $value, $type = 'string')
    {
        if (array_key_exists($type, self::$types))
        {
            $type = self::$types[$type];
        }
        if (!in_array($type, self::$types))
        {
            return false;
        }
        try
        {
            Bgx_Core::database()->insert('settings', array('var' => $id, 'value' => $value, 'type' => $type));
            $this->data[$id] = $value;
        }
        catch (Exception $e)
        {
            Bgx_Core::log(__METHOD__ . ' cant add new setting: ' . $e->getMessage(), Zend_Log::ERR);
        }
    }

    public function getAll()
    {
        return $this->data;
    }

    public function reload()
    {
        $this->_load();
    }
}