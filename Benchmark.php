<?php
/**
 * @author	Jens Sage <j.sage@bgx-mail.de>
 * 
 * @todo untested... do sth against it
 */
class Bgx_Benchmark implements Comparable
{
	private $_result_average_time = 0.0;
	
	private $script = '';
	
	/**
	 * @param	string			$pathToPhp
	 * @return	Bgx_Benchmark
	 */
	public function setScriptFromFile($pathToPhp)
	{
		if (file_exists($pathToPhp))
		{	
			$this->script = file_get_contents($pathToPhp);
		}
		else
		{
			throw new FileNotFoundException("File '{$pathToPhp}' does not exist.");
		}
		return $this;
	}
	
	/**
	 * @param	string			$string
	 * @return	Bgx_Benchmark
	 */
	public function setScript($string)
	{
		$this->script = $string;
		return $this;
	}
	
	/**
	 * @param	int	$times
	 * @return	Bgx_Benchmark
	 */
    public function benchmark($times = 100)
    {
		@set_time_limit(500);
		
		for ($i = 0; $i < $times; $i++)
		{
			$stop = 0.0;
			$start = microtime(true);
			
			eval($this->script);
			
			$stop = microtime(true);
			$exec_time = $stop-$start;
			$this->_result_average_time = ($this->_result_average_time + $exec_time) / 2;
		}
		
		@set_time_limit(intval(ini_get('max-execution-time')));
		return $this;
    }
    
    public function getAverageExecTimeInMs()
    {
    	return $this->_result_average_time*1000;
    }

    public function equals(self $comparableObj)
    {
    	return $this->_result_average_time == $comparableObj->_result_average_time;
    }
    
	public function compareTo(self $comparableObj)
    {
    	//return $this->_result_average_time == $comparableObj->_result_average_time;
    }
}