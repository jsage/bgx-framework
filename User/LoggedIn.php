<?php
/**
 * Enter description here...
 *
 */
class Bgx_User_LoggedIn extends Bgx_User_Abstract
{
    public function __construct($data)
    {
        $this->id        = is_object($data) ? $data->id        : (is_array($data) ? $data['id']        : 0);
        $this->username  = is_object($data) ? $data->username  : (is_array($data) ? $data['username']  : 0);
        $this->email     = is_object($data) ? $data->email     : (is_array($data) ? $data['email']     : "");

        if (!$this->_DATA = Bgx_Core::cache()->load(sprintf($this->cache_identifier['data'], $this->id)))
        {
            $this->_DATA = Bgx_Core::database()->fetchRow(Bgx_Core::database()->select()->from('users')
                                                          ->where(Bgx_Core::database()->quoteIdentifier('id') . ' = ?',
                                                                  $this->id));
            $this->lastactivity = Zend_Date::now();

            #$this->language              = !empty($this->_DATA['language'])
            #                               ? $this->_DATA['language']
            #                               : Bgx_Core::translate()->getNumByIdentifier(Bgx_Core::locale()->getLanguage());
            
            Bgx_Core::cache()->save($this->_DATA, 'user-' . $this->id);
        }
    }
    
    public function __destruct()
    {
    	Bgx_Core::getDb()->query('UPDATE users SET lastactivity = NOW() WHERE id = ' . $this->id);
    }

    public function __set($name, $value)
    {
        try
        {
            Bgx_Core::database()->update('users',
                                         array($name => is_bool($value) ? new Zend_Db_Expr($value) : $value),
                                         Bgx_Core::database()->quoteIdentifier('id') .
                                         Bgx_Core::database()->quoteInto(' = ?', $this->id, Zend_Db::INT_TYPE));
            if (isSet($this->$name))
            {
                $this->$name = $value;
            }
            if (isSet($this->_DATA[$name]))
            {
                $this->_DATA[$name] = $value;
            }
        }
        catch (Exception $e)
        {
            Bgx_Core::log('cant set user-field ' . $name . '. throwed: ' . $e->getMessage(), Zend_Log::WARN);
            return false;
        }
    }

    /**
     * @return array
     */
    public function getGroups()
    {
        if (!$this->groups_cache = Bgx_Core::getCache()->load(sprintf($this->cache_identifier['groups'], $this->id)))
        {
             $this->groups_cache = Bgx_Core::getDb()->query("SELECT group_id AS id FROM users_to_groups WHERE user_id = {$this->id}");
        }
        return $this->groups_cache;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->_DATA))
        {
            return $this->_DATA[$name];
        }
        return false;
    }

	public function deleteCache()
	{
		Bgx_Core::cache()->clear('user-' . $this->id);
	}
}