<?php
/**
 * User
 * @author    Jens Sage <j.sage@babbls.de>
 * @package   BgxCms
 */
abstract class Bgx_User_Abstract
{
	public		$id = 0,
				$data;
	protected	$groups_cache = array();
	 
	/**
	 * @param	array	$data
	 */
	public abstract function __construct(array $data);

	/**
	 * sets a usersetting
	 *
	 * @param  string $name
	 * @param  mixed  $value
	 * @return bool   everything ok?
	 */
	public abstract function __set($name, $value);

	/**
	 * returns optional user-information
	 *
	 * @param  string $name
	 * @return bool|mixed
	 */
	public abstract function __get($name);

	/**
	 * @param	string				$username
	 * @return	Bgx_User_Abstract
	 * @throws	Bgx_Exceptions
	 */
	public static function getUser($username)
	{
		return (Bgx_Core::currentUser()->username == $username) ?
				Bgx_Core::currentUser() :
				new Bgx_User_Watch(array('username' => $username));
	}

	/**
	 * @param	int					$uid
	 * @return	Bgx_User_Abstract
	 * @throws	Exceptions
	 */
	public static function getUserById($uid)
	{
		return (Bgx_Core::currentUser()->id == $uid) ?
		Bgx_Core::currentUser() :
		new Bgx_User_Watch(array('id' => $uid));
	}

	/**
	 * @return	array
	 *
	 */
	public abstract function getGroups();

	/**
	 * @return	void
	 */
	public function updateUserActivity()
	{}

	/**
	 * @return	array
	 */
	public function toArray()
	{
		$return = array('id'		=> $this->id,
        	   		    'username'	=> $this->username,
        			    'email'		=> $this->email,
        			    'language'	=> $this->language,
        				'reg_date'	=> $this->reg_date,
        				'online'	=> $this->online);

		return $return;
	}

	public function isAuthorisedTo($action)
	{
		foreach ($this->getGroups() AS $group)
		{
			//if ($group->isAuthorisedTo($action))
			//return true;
		}
		 
		return false;
	}
}