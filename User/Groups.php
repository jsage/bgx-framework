<?php

class Bgx_User_Groups
{
    private static $groups = array();
    private static $loaded = false;
    
    private static function _fetchGroups()
    {
        // load the cache
        $result = Bgx_Core::getDb()->query("SELECT id, name FROM groups"); 
        while($r = $result->fetch())
        {
        	self::$groups[$r['id']] = $r['name'];
        }
    }
    
    public static function getGroups()
    {
        if (!self::$loaded)
        {
            self::_fetchGroups();
        }
        return self::$groups;
    }
    
    public static function getGroupIdByName($name)
    {
        if (!self::$loaded)
        {
            self::_fetchGroups();
        }
        
        return array_search($name, self::$groups);
    }
    
    /**
     * @return int ID of guest-group
     */
    public static function getGuestGroup()
    {
        return self::getGroupIdByName('guests');
    }
    
    /**
     * @param  int $group_id
     * @return array
     */
    public static function getGroupMembers($group_id)
    {
        
    }
}