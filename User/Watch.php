<?php
/**
 *
 *
 */
class Bgx_User_Watch extends Bgx_User_Abstract
{
	/**
	 * @param	array	$data
	 * @return	void
	 *
	 * @throws	Exception
	 */
	public function __construct(array $data)
	{
		#if (!$data = $this->_getUserDataFromCache(isSet($data['id']) ? $data['id'] : 0))
		#{
			$sel = Bgx_Core::database()->select()->from('users');
			if (isSet($data['id']))
			{
				$sel->where('id = ?', $data['id'], Zend_DB::INT_TYPE);
			}
			else if (isSet($data['username']))
			{
				$sel->where('username = ?', $data['username']);
			}
			else
			{
				throw new Exception('Parameters missing to fetch a user.');
			}

			$data = Bgx_Core::database()->fetchRow(&$sel);
		#}
			if ($data == false)
			{
				throw new Exception('User not found.', 19001);
			}
			foreach ($data AS $k => $v)
			{
				$this->$k = $v;
			}


		$this->lastactivity = new Zend_Date(is_null($this->lastactivity) ? 0 : strtotime($this->lastactivity));
		$this->online = $this->lastactivity->compare(6, Zend_Date::MINUTE) ? false : true;

		Syndicat_Observer::GetInstance()->Notify(__METHOD__, array('uObj' => &$this));
	}

	/**
	 * senseless shiat = warnem�nde
	 *
	 * @param	string	$name
	 * @param	mixed	$value
	 */
	public function __set($name, $value) {

	}

	/**
	 * @return	array
	 */
	public function getGroups()
	{
		if (!$this->groups_cache = Bgx_Core::cache()->load(sprintf($this->cache_identifier['groups'], $this->id)))
		{
			$this->groups_cache = Bgx_Core::database()->fetchAll(Bgx_Core::database()->select()->from('users_to_groups',
			array('id' => 'group_id'))
			->where(Bgx_Core::database()->quoteIdentifier('user_id') . ' = ?',
			$this->id),
			array(),
			Zend_DB::FETCH_ASSOC);
			Bgx_Core::cache()->save($this->groups_cache, sprintf($this->cache_identifier['groups'], $this->ID));
		}
		return $this->groups_cache;
	}

	/**
	 * @param	string	$name
	 * @return	mixed
	 */
	public function __get($name)
	{
		if (isSet($this->$name))
		{
			return $this->$name;
		}
		if (isSet($this->_DATA[$name]))
		{
			return $this->_DATA[$name];
		}
	}
}