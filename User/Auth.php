<?php
/**
 * Auth
 * @author  Jens Sage <j.sage@babbls.de>
 * @package BgxCms
 */
class Bgx_User_Auth
{
    const IDENT_ID = 0;
    const IDENT_USERNAME = 1;
    const IDENT_EMAIL = 2;

    /**
     * @param mixed  $identity
     * @param string $password
     * @param bool|Bgx_User_LoggedIn   $identity_type
     */
    public static function login($identity, $password, $identity_type = null)
    {
    	if (self::isLoggedIn())
    	{
    		return false;
    	}

        switch ($identity_type)
        {
            case self::IDENT_ID:
                $identity_type = 'id';
                break;
            case self::IDENT_EMAIL:
                $identity_type = 'email';
                break;
            default:
                $identity_type = 'username';
        }

        $password = md5($password);
        $identity = Bgx_Core::database()->quote($identity);

        if (!$res = Bgx_Core::database()->query(
        	"SELECT id, username, email FROM users " .
        	"WHERE (active = true) AND ({$identity_type} = {$identity}) AND (password = '{$password}')")->fetchObject()
        )
        {
        	return false;
        }

        return $_SESSION['user'] = new Bgx_User_LoggedIn($res);
    }

    /**
     * @return bool
     */
    public static function isLoggedIn()
    {
        return isset($_SESSION['user']);
    }

    /**
     * @return void
     */
    public static function logout()
    {
        if (self::isLoggedIn())
        {
        	$_SESSION['user']->updateUserActivity();
            unset($_SESSION['user']);
        }
    }

    /**
     * @return Bgx_User_Abstract
     */
    public static function getUserObj()
    {
        return isset($_SESSION['user']) ? $_SESSION['user'] : new Bgx_User_LoggedOut();
    }
}