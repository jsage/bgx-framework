<?php

class Bgx_User_LoggedOut extends Bgx_User_Abstract
{

    /**
     *
     */
    public function __construct()
    {
        $this->username  = 'guest'; # Because of that, locale init fails with Bgx_Core::translate()->translate('core.guest');

        #$this->language  = Bgx_Core::translate()->getNumByIdentifier(Zend_Locale::BROWSER);

    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $_SESSION['guest'][$name] = $value;
    }

    /**
     * @return array
     */
    public function getGroups()
    {
        return array(1 => array('id' => Bgx_User_Groups::getGuestGroup()));
    }

    /**
     * @param  string $name
     * @return mixed
     */
    public function __get($name)
    {
        return isSet($_SESSION['guest'][$name]) ? $_SESSION['guest'][$name] : null;
    }
}