<?php
/**
 * Register
 * @author    Jens Sage <j.sage@babbls.de>
 * @package   BgxCms
 */
class Bgx_User_Register
{
	const USERNAME_TOO_LONG = 5;
	const INVALID_EMAIL_SYNTAX = 4;
	const USERNAME_EXISTS = 3;
	const EMAIL_EXISTS = 2;
	const REGISTER_SUCCEED = 1;

	const MAX_USERNAME_LENGTH = 16;

	public static function isValidMailSyntax($email)
	{
		$validator = new Zend_Validate_EmailAddress();
		return $validator->isValid($email);
	}

	public static function isValidUsername($uname)
	{
		// TODO implement
	}

	/**
	 * check, if a username already exists
	 *
	 * @param string $value
	 * @return bool
	 */
	public static function usernameExists($value)
	{
		if (empty($value) || strlen($value) > self::MAX_USERNAME_LENGTH)
		{
			return false;
		}
		return (bool) Bgx_Core::database()->fetchOne(
		Bgx_Core::database()->select()->from('users',
		array(new Zend_Db_Expr('COUNT(*)')))
		->where(Bgx_Core::database()->quoteIdentifier('username') . ' = ?', $value));
	}

	/**
	 * check, if an email-adress is already in use
	 *
	 * @param string $adress
	 * @return bool
	 */
	public static function useremailExists($value)
	{
		if (empty($value))
		{
			return false;
		}
		return (bool) Bgx_Core::database()->fetchOne(
		Bgx_Core::database()->select()->from('users',
		array(new Zend_Db_Expr('COUNT(*)')))
		->where(Bgx_Core::database()->quoteIdentifier('email') . ' = ?', $value));

	}

	/**
	 * registers a new user
	 *
	 * @param  string $username
	 * @param  string $email
	 * @param  string $password
	 * @return bool
	 */
	public static function register($username, $email, $password, $send_mail_to_user = true)
	{
		$username = trim($username);
		$password = trim($password);
		if (self::usernameExists($username))
		{
			return self::USERNAME_EXISTS;
		}
		else if (self::useremailExists($email))
		{
			return self::EMAIL_EXISTS;
		}
		else if (strlen($username) > self::MAX_USERNAME_LENGTH)
		{
			return self::USERNAME_TOO_LONG;
		}
		else if (!self::isValidMailSyntax($email))
		{
			return self::INVALID_EMAIL_SYNTAX;
		}
		else
		{
			try
			{
				$insert = array(
                	'username' => $username,
                	'password' => md5($password),
                	'email' => $email);

				if (!empty(Bgx_Core::currentUser()->language))
				{
					$insert['language'] = Bgx_Core::currentUser()->language;
				}

				$res = Bgx_Core::database()->insert('users', $insert);
				if ($res)
				{
					$uId = Bgx_Core::database()->lastInsertId();
					$res = Bgx_Core::database()->insert('users_to_groups',
					array('user_id'  => $uId,
                                                              'group_id' => Bgx_User_Groups::getGroupIdByName('users')));

					Bgx_Core::database()->insert('register_codes', array('user_id' => $uId, 'code' => self::createCode()));

					Bgx_Event::fire(__METHOD__, array('uId' => $uId, 'user' => $insert));
					
					if ($send_mail_to_user)
					{
						Bgx_MailTemplate::getInstance()->newMail('registration_step1',
						array('username' => $insert['username']))
						->addTo($insert['email'])
						->send();
					}
					return self::REGISTER_SUCCEED;
				}
			}
			catch (Exception $e)
			{
				Bgx_Core::log(__METHOD__ . ': cant insert, trowed "'.$e->getMessage().'"', Zend_Log::ERR);
				return false;
			}
		}
	}

	/**
	 * inviting an user
	 *
	 * @param sring $email
	 * @param string $usertext
	 */
	public static function invite($email, $usertext)
	{
		Bgx_MailTemplate::getInstance()->newMail('invite_user',
		array('usertext' => $usertext))
		->addTo($email)
		->send();
	}

	/**
	 * @param  string $code
	 * @return bool
	 */
	public static function activate($code)
	{
		if (!self::codeExists($code))
		{
			return false;
		}
		try
		{
			$uId = (int) Bgx_Core::database()->fetchOne(
			Bgx_Core::database()->select('register_code', array('user_id'))
			->where('code = ?', $code));
			Bgx_Core::database()->delete('register_code',
			Bgx_Core::database()->quoteInto('code = ?', $code));
			return Bgx_Core::database()->update('users',
			array('active' => new Zend_Db_Expr('true')),
			Bgx_Core::database()->quoteInto('id = ?', $uId, Zend_Db::INT_TYPE));
		}
		catch (Exception $e)
		{
			Bgx_Core::log('cant activate user:' . $e->getMessage());
			return false;
		}
	}

	/**
	 * @param  string $code
	 * @return bool
	 */
	public static function codeExists($code)
	{
		return (bool) Bgx_Core::database()->fetchOne(
		Bgx_Core::database()->select('register_codes', array(new Zend_Db_Expr('COUNT(*)')))
		->where('code = ?', $code));
	}

	/**
	 * @return string
	 */
	public static function createCode()
	{
		do
		{
			$code = uniqid(rand(0,1000));
		} while (self::codeExists($code)); //�@todo replace this wastage of making those queries

		return $code;
	}
}