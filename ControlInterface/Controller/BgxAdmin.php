<?php

class BgxAdminController extends Bgx_Controller_Abstract
{
	public function __construct()
	{
		$this->_view()->addTemplateDir(LIBRARY . '/Bgx/ControlInterface/Templates/');
		$this->_view()->assign('messages', array());
	}
	public function bgxAdminAction()
	{
		$this->_view()->display('bgxadmin');
	}
	
	
	public function groupsAction()
	{
		$this->_view()->assign('groups', 
			Bgx_Core::getDb()->query("SELECT * FROM groups")
				->fetchAll(PDO::FETCH_ASSOC)
		);
		
		$this->_view()->display('bgxadmin_groups');
	}
	public function mailsAction()
	{
		if (isset($_POST['mail']))
		{
			$mail = Bgx_Core::getDb()->quote($_POST['mail'], PDO::PARAM_STR);
			$r = Bgx_Core::getDb()->query("INSERT INTO email_blacklist (email) VALUES ($mail)");
			
			$msg = $this->_view()->getVar("messages");
			$msg[] = $r ? tr("Added mail to list") : tr("Error occured. Please try again");
			$this->_view()->assign("messages", $msg);
		}
		
		$this->_view()->assign(
			'blacklist_table',
			$this->_view()->dataTable()->setData(
				Bgx_Core::getDb()->query("SELECT id, email FROM email_blacklist")
			)->setEditable(true)->setIdentifierColumn('id')->setEditableColumns(array('email'))
		);
		$this->_view()->display('bgxadmin_mails');
	}
	
	public function languagesAction()
	{
		$this->_view()->assign(
			'languages',
			Bgx_Core::getDb()->query("SELECT id, identifier, active FROM languages")
			->fetchAll(PDO::FETCH_ASSOC)
		);
		
		if (isset($_GET['lang']))
		{
			/**
			 * @var Bgx_View_Helper_DataTable
			 */			
			$trans_tbl = $this->_view()->dataTable();
			$trans_tbl->setData(
				Bgx_Core::getDb()->query(
					"SELECT lvt.id, lv.key, lvt.translation FROM language_vars AS lv " . 
					"INNER JOIN language_vars_translated AS lvt ON lvt.key_id = lv.key_id " .
					"WHERE lvt.lang_id = " . ((int) $_GET['lang'])
				)
			);
			$trans_tbl->setLabels(
				array(
					'id' => '#',
					'key' => tr("Original"), 
					'translation' => tr("Translation")
				)
			);
			$trans_tbl->setEditable(true);
			$trans_tbl->setIdentifierColumn('id');
			
			$observer = new LanguageEventObserver();
			$observer->attach($trans_tbl->getEventSubject());
			
			$this->_view()->assign('translations_table', $trans_tbl);
		}
		
		$this->_view()->display('bgxadmin_languages');
	}
}