<?php
	
	if (isset($_POST['username']))
	{
		$create_data = array(
			'username' => $_POST['username'],
			'email' => $_POST['email'],
			'password' => $_POST['password'],
			'active' => isset($_POST['active']) ? true : false,
			'language' => $_POST['language'],
			'sent' => true,
			'invalid' => array()
		);
		
		if (trim($_POST['username']) == "") // @todo allowed charts 
		{
			$create_data['invalid']['username'] = "empty";
		}
		
		if (trim($_POST['email']) == "") // @todo allowed charts 
		{
			$create_data['invalid']['email'] = "empty";
		}
		
		if (trim($_POST['password']) == "") // @todo allowed charts 
		{
			$create_data['invalid']['password'] = "empty";
		}
		
		if (!count($create_data['invalid']))
		{
			$username = Bgx_Core::getDb()->quote($_POST['username'], PDO::PARAM_STR);
			$email = Bgx_Core::getDb()->quote($_POST['email'], PDO::PARAM_STR);
			$password = Bgx_Core::getDb()->quote(md5(trim($_POST['password'])), PDO::PARAM_STR);
			$active = Bgx_Core::getDb()->quote(isset($_POST['active']) ? true : false, PDO::PARAM_BOOL);
			$language = Bgx_Core::getDb()->quote($_POST['language'], PDO::PARAM_INT);
			
			$res = Bgx_Core::getDb()->query(
				"INSERT INTO users (username, email, password, active, language) " .
				"VALUES ($username, $email, $password, $active, $language)"
			);
			
			$this->_view()->assign('inserted', (bool) $res);
		}
	}
	else
	{
		$create_data = array(
			'username' => '', 
			'email' => '', 
			'password' => '',
			'active' => true,
			'language' => 0,
			'sent' => false,
			'invalid' => array());
		for ($i = 0; $i < 5; ++$i)
		{
			$s = 'abcdefghikmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVW23456789';
			$create_data['password'] .= substr($s, (rand() % (strlen($s))), 1);
		}
	}
	$this->_view()->assign('create', $create_data);
	
	$id_from = (int) isset($_GET['id_from']) ? $_GET['id_from'] : 0;
	$id_to = (int) isset($_GET['id_to']) ? $_GET['id_to'] : 0;
	if ($id_to < $id_from) 
		$id_to = 0;
	$id_limit = $id_to - $id_from;
	
	if ($id_to)
	{
		$this->_view()->assign(
			'users', 
			Bgx_Core::getDb()->query(
				"SELECT id, username, active, email FROM users OFFSET ".
				"{$id_from} LIMIT {$id_limit}"
			)->fetchAll(PDO::FETCH_ASSOC)
		);
	}
	
	$this->_view()->assign('id_from', $id_from);
	$this->_view()->assign('id_to', $id_to);
	$this->_view()->display('bgxadmin_users');