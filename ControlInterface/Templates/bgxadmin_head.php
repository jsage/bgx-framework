<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"><head>
	<head>
		<style>
			* {
				font-family:sans-serif;
				color:buttontext;
				font-size:13px;
				text-decoration:none;
				margin:0;
				border:0;
				padding:0;
			}
			
			input {
				border: 1px solid #c0c0c0;
				padding:3px;
				margin: 0 10px;
				background: #f0f0f0;
			}
			
			html,body {
				background:windowframe;
				height:100%;
			}
			
			h1, h2, h3, h4 {
				font-size:16px;
				margin-bottom:15px;
			}
			
			h2 {
				font-size:14px;
			}
			
			a {
				border-bottom: 1px solid #111;
				color:highlighttext;
			}
			
			a:hover {
				background:highlight;
			}
			
			#foot {
				margin:25px 0 0 0;
				text-align:center;
			}
			
			#wrapper {
				width:600px;
				background:window;
				padding:10px;
				border-right:1px solid windowtext;
			}
			
			.invalid {
				background:red;
			}
			
			fieldset {
				padding:10px;
				margin-bottom:15px;
				border: 1px solid #c0c0c0
			}
			
			legend {
				padding: 0 10px 0 10px;
				font-weight:bold;
			}

			table {
				background:buttonface;
				width:100%;
				table-spacing:0px;
				margin: 10px 0;
			}
			
			td, th {
				padding:5px;
			}
			
			th {
				background:buttonhighlight;
			}
			
			.row1 td, .row2 td{
				border-top: 1px solid #fff;
				border-left: 1px solid #fff;
				border-bottom: 1px solid #c0c0c0;
				border-right: 1px solid #c0c0c0;
			}
			
			.row1 {
				background: #f0f0f0;
			}
			
			.row2 {
				background: #cfcfcf;
			}
			
			#foot {
				color:#444;
			}
			
			#bgxmenu tr td {
				font-size:16px;
				font-weight:bold;
				
			}
		</style>
		<script type="text/javascript">
		<?php if (count($this->messages)) { foreach ($this->messages AS $msg) { ?>
			alert("<?php echo addslashes($msg);?>");
		<?php } } ?>
		
			function toggle(elem) {
				var el = document.getElementById(elem);
				if (el.style.display=='none')
				{
					el.style.display='block';
				}
				else
				{
					el.style.display='none';
				}
			}
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<link rev="made" href="BerlinGrafix CMS" />
		<title><?=Bgx_Core::index('sitename'); ?> - Control Interface</title>
	</head>
	<body>
		<div id="wrapper">
		<a href="<?=$this->url?>/bgxadmin/"><h1><?php echo tr("Control Interface"); ?></h1></a>