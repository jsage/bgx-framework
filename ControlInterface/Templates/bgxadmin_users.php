<?=$this->includeTpl('bgxadmin_head'); ?>

<form method="get" action="<?=$this->url?>/bgxadmin/users">
	<fieldset>
		<legend><a href="#" onclick="toggle('filter')">Filter</a></legend>
		<table id="filter">
			<tr>
				<td>ID</td>
				<td>from <input type="text" value="<?php echo $this->id_from; ?>" name="id_from"/></td>
				<td>to <input type="text" value="<?php echo $this->id_to; ?>" name="id_to"/></td>
				<td><input type="submit" value="filter"/></td>
			</tr>
		</table>
	</fieldset>
</form>


<form method="post" action="<?php echo $this->url ?>/bgxadmin/users">
	<fieldset>
		<legend><a href="#" onclick="toggle('adduser')">Create User</a></legend>
		<?php if ($this->inserted)
		{ ?>
			<p>User created</p>
		<?php } else { ?>
		<div id="adduser" <?php if (!$this->create['sent']) { ?>style="display:none"<?php } ?>>
			<table>
				<tr <?php if (isset($this->create['invalid']['username'])) {
					echo "class='invalid'"; } ?>>
					<td><?php echo tr('Username', true); ?></td>
					<td><input type="text" value="<?php
						echo htmlentities($this->create['username']);
					?>" name="username"/></td>
				</tr>
				<tr <?php if (isset($this->create['invalid']['email'])) {
					echo "class='invalid'"; } ?>>
					<td><?php echo tr('E-Mail', true); ?></td>
					<td><input type="text" value="<?php
						echo htmlentities($this->create['email']);
					?>" name="email"/></td>
				</tr>
				<tr <?php if (isset($this->create['invalid']['password'])) {
					echo "class='invalid'"; } ?>>
					<td><?php echo tr('Password', true); ?></td>
					<td><input type="text" value="<?php
						echo htmlentities($this->create['password']);
					?>" name="password"/></td>
				</tr>
				<tr>
					<td><?php echo tr('Active', true); ?></td>
					<td><input type="checkbox" name="active" <?php 
						echo ($this->create['active']) ? 'checked="checked"' : "";
					?>/></td>
				</tr>
				<tr>
					<td><?php echo tr('Language', true); ?></td>
					<td><select name="language">
					<?php 
						foreach (Bgx_Core::translate()->getLanguages() AS $id => $lang)
						{
							$sel = ($this->create['language']==$id) ? 'selected="selected"' : "";
							echo "<option value=\"{$id}\" {$sel}>$lang</option>";
						}
					?></select></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="<?php echo tr("create", true);?>"/></td>
				</tr>
			</table>
		</div>
		<?php } ?>
	</fieldset>
</form>

<table>
	<tr>
		<th><?php echo tr('#', true); ?></th>
		<th><?php echo tr('Username', true); ?></th>
		<th><?php echo tr('E-Mail', true); ?></th>
		<th colspan="2"><?php echo tr('Actions', true); ?></th>
	</tr>
<?php 
	if (count($this->users)) {
		$n=2;
		foreach($this->users AS $user) { 
			$n=($n==1) ? 2 : 1;
?>
	<tr class="row<?php echo $n; ?>">
		<td rowspan="2"><?php echo $user['id']; ?></td>
		<td><?php echo $user['username']; ?></td>
		<td><?php echo $user['email']; ?></td>
		<td rowspan="2">
			<a href="<?php echo $this->url; ?>/bgxadmin/users/edit/<?php echo $user['id'] ?>">
				<?php echo tr('edit', true); ?>
			</a>
		</td>
		<td rowspan="2">
			<a href="<?php echo $this->url; ?>/bgxadmin/users/delete/<?php echo $user['id'] ?>">
				<?php echo tr('delete', true); ?>
			</a>
		</td>
	</tr>
	<tr class="row<?php echo $n; ?>">
		<td colspan="2"><?php echo $user['active'] ? tr('active', true) : tr('inactive', true); ?></td>
	</tr>
<?php } }?>
</table>

<?=$this->includeTpl('bgxadmin_foot'); ?>