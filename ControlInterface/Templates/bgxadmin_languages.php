<?=$this->includeTpl('bgxadmin_head'); ?>


<h2>Languages</h2>
<?php echo $this->dataTable()->setData($this->languages)->setEditable(true); ?>

<h2>Translate...</h2>

<form method="get" action="<?php echo $this->url ?>/bgxadmin/languages">
	<table>
		<tr>
			<th colspan="2">Choose a language to translate</th>
		</tr>
		<tr>
			<td>
				<select name="lang">
					<?php 
					foreach ($this->languages AS $row) { 
					echo '<option value="' . $row['id'] . '">' . $row['identifier'] . '</option>';
					 } ?>
				</select>
			</td>
			<td>
				<input type="submit" value="choose"/>
			</td>
		</tr>
	</table>
</form>

<?php 
echo $this->translations_table;
?>

<?=$this->includeTpl('bgxadmin_foot'); ?>