<?php
/**
 * @author  Jens Sage <j.sage@bgx-mail.de>
 * @package BgxCms
 */
class Bgx_Language_Manage
{
	/**
	 * imports an lang-file
	 * @param string $lang_file_content
	 */
	public static function import($lang_file_content)
	{
		
	}
	
	/**
	 * exports into a lang-file
	 * @return string
	 */
	public static function export()
	{
		
	}
	
	/**
	 * @param string $identifier
	 */
	public static function addLanguage($identifier)
	{
		
	}
	
	/**
	 * @param int $id
	 */
	public static function deleteLanguageById($id)
	{
		
	}
	
	/**
	 * @param string $identifier
	 */
	public static function deleteLanguageByIdentifier($identifier)
	{
		
	}
	
	/**
	 * @param string $name
	 */
	public static function addVar($name)
	{
		
	}
	
	/**
	 * @param int $id
	 */
	public static function deleteVarById($id)
	{
		
	}
}