<?php
/**
 * @author  Jens Sage <j.sage@bgx-mail.de>
 * @package BgxCms
 */
class Bgx_Gimmicks
{
	/**
	 * @copyright Tango Desktop Project <http://tango.freedesktop.org>
	 */
	private static $colorpalette = array( 'butter1' => '#fce94f',
			                              'butter2' => '#edd400',
			                              'butter3' => '#c4a000',
		                                  'orange1' => '#fcaf3e',
		                                  'orange2' => '#f5f900',
		                                  'orange3' => '#ce5c00',
		                                  'chocolate1' => '#e9596e',
		                                  'chocolate2' => '#c17d11',
		                                  'chocolate3' => '#8f5902',
		                                  'chameleon1' => '#8ae234',
		                                  'chameleon2' => '#73d216',
		                                  'chameleon3' => '#4e9a06',
		                                  'skyblue1' => '#729fcf',
		                                  'skyblue2' => '#3465a4',
		                                  'skyblue3' => '#204a87',
		                                  'plum1' => '#ad7fa8',
		                                  'plum2' => '#75507b',
		                                  'plum3' => '#5c3566',
		                                  'scarletred1' => '#ef2929',
		                                  'scarletred2' => '#cc0000',
		                                  'scarletred3' => '#a40000',
		                                  'aluminium1' => '#eeeeec',
		                                  'aluminium2' => '#d3d7cf',
		                                  'aluminium3' => '#babdb6',
		                                  'aluminium4' => '#888a85',
		                                  'aluminium5' => '#555753',
		                                  'aluminium6' => '#2e3436'
	                                     );

    /**
     * @param  int $date timestamp
     * @return int       age
     */
    public static function getAgeByDate($date)
    {
        $age = time() - strtotime($date);
        $age = floor($age / 60 / 60 / 24 / 365);
        return $age;
    }

    /**
     * @param  int $year
     * @return array
     */
    public static function getYearsFrom($year = 1925)
    {
        $current = date('Y');
        $years = array();
        for ($i = &$current; $i > $year; --$i)
        {
            $years[]['year'] = $i;
        }
        return $years;
    }

    /**
     * @param  int $month
     * @return array
     */
    public static function getMonthDays($month = 1)
    {
        $days = array();
        for ($i = 1; $i <= 31; ++$i)
        {
            $days[]['day'] = $i;
        }
        return $days;
    }

    /**
     * @return unknown
     */
    public static function getMonths()
    {
        $months = array();
        for ($i = 1; $i <= 12; ++$i)
        {
            $months[$i]['month'] = $i;
            $months[$i]['month_translation'] = new Zend_Date($i, Zend_Date::MONTH_SHORT);
            $months[$i]['month_translation'] = $months[$i]['month_translation']->toString('MMMM');
        }
        return $months;
    }

    /**
     * @param Zend_Date $date
     */
    public static function humanDate(Zend_Date $date)
    {
        $output = "";

        if ($date->isLater(new Zend_Date(time() - 15*60)))
        {
        	$output .= tr('vor ein paar Minuten');
        }
    	else if ($date->isLater(new Zend_Date(time() - 60*60)))
        {
        	$output .= tr('in der letzten Stunde');
        }
    	else if ($date->isLater(new Zend_Date(time() - 60*60*4)))
        {
        	$output .= tr('vor ein paar Stunden');
        }
        else if ($date->isToday())
        {
            $output .= tr('heute') . $date->toString(' a');
        }
        else if ($date->isYesterday())
        {
            $output .= tr('gestern') . $date->toString(' a');
        }
        else if ($date->isTomorrow())
        {
            $output .= tr('morgen') . $date->toString(' a');
        }
    	else if ($date->isLater(new Zend_Date(time() - 60*60*24*7)))
        {
            $output .= $date->toString('EEEE a');
        }
		else
		{
			$output .= $date->toString('d. MMMM YYY');
		}

		return $output;
    }

    /**
     * @
     */
    public static function listBgxColorSet()
    {

    }

    /**
     * @param  string $name
     * @return string|null
     */
    public static function getColor($name = 'aluminium6')
    {
        return isSet(self::$colorpalette[$name]) ? self::$colorpalette[$name] : null;
    }
}