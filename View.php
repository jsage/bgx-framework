<?php
/**
 * @author Jens Sage <j.sage@bgx-mail.de>
 *
 */
class Bgx_View
{
	private $_controllerObj;
	
	private $_templateDirs = array();
	
	private $_vars = array();
	
	private $_helpers			= array();
	private $_defaultHelpers	= null;
	
	public function __construct(Bgx_Controller_Abstract $controllerObj)
	{
		$this->_controllerObj = $controllerObj;
		
		if (Bgx_Core::index('template_dir'))
		{
			$this->addTemplateDir(Bgx_Core::index('template_dir'));
		}
		
		$this->_vars = array(
			'url' => Bgx_Core::getRequest()->getBaseUrlString(),
			'lang' => Bgx_Core::translate()->getLanguage(),
			'loggedIn' => Bgx_User_Auth::isLoggedIn(),
			'user' => Bgx_User_Auth::getUserObj()
		);
	}
	
	/**
	 * Register a template helper
	 *
	 * @param	string		$helper_class_name
	 * @return	Bgx_View
	 */
	public function registerHelper($helper_class_name)
	{
		if (class_exists($helper_class_name))
		{
			$this->_helpers[] = $helper_class_name;
		}
		else
		{
			throw new Exception("Helper-Class '{$helper_class_name}' does not exist");
		}
		
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getDefaultHelpers()
	{
		if ($this->_defaultHelpers == null)
		{
			if ($handle = opendir(LIBRARY . '/Bgx/View/Helper/'))
			{
			    while (false !== ($file = readdir($handle)))
			    {
			        if ($file != '.' && $file != '..' && $file != 'Abstract.php')
			        {
						$this->_defaultHelpers[] = str_replace('.php', '', $file);
			        }
			    }
			    closedir($handle);
			}
			else
			{
				throw new Exception("Cant open default helpers dir");
			}
		}
		
		return $this->_defaultHelpers;
	}
	
	/**
	 * returns path to given tempalte
	 *
	 * @param	string	$template_name
	 * @return	string	path to template
	 */
	private function _getTemplatePath($template_name)
	{
		foreach ($this->_templateDirs AS $dir)
		{
			if (file_exists($dir . '/' . $template_name . '.php'))
			{
				return $dir . '/' . $template_name . '.php';
			}
		}
		
		throw new Exception("Template '{$template_name}' doesn\'t exist");
	}
	
	/**
	 * @param	string $dir
	 * @return	Bgx_View
	 */
	public function addTemplateDir($dir)
	{
		if (file_exists($dir))
		{
			$this->_templateDirs[] = $dir;
		}
		
		return $this;
	}
	
	/**
	 * Assigns a variable to the template
	 *
	 * @param	int|string	$key
	 * @param	mixed		$value
	 * @return	Bgx_View
	 */
	public function assign($key, $value)
	{
		if (is_string($key) || is_int($key))
		{
			$this->_vars[$key] = $value;
		}
		else
		{
			throw new Exception();
		}
		return $this;
	}
	
	public function getVar($key)
	{
		return isset($this->_vars[$key]) ? $this->_vars[$key] : null;
	}
	
	/**
	 * Prints the generated output
	 *
	 * @param	string	$template_name
	 * @return	Bgx_View
	 */
	public function display($template_name)
	{
		ob_start();
		
		include $this->_getTemplatePath($template_name);
		
		echo ob_get_clean();
		
		return $this;
	}
	
	/**
	 * This is meant to be used in templates
	 *
	 * @param	string	$template_name
	 */
	public function includeTpl($template_name)
	{
		include $this->_getTemplatePath($template_name);
	}
	
	/**
	 * @param	string	$name
	 * @param	array	$arguments
	 * @return	Bgx_View_Helper_Abstract
	 */
	public function __call($name, $arguments)
	{
		$name = ucfirst($name);
		if (in_array($name, $this->_helpers))
		{
			return new $name(&$this, $arguments);
		}
		else if (in_array($name, $this->getDefaultHelpers()))
		{
			$name = 'Bgx_View_Helper_' . $name;
			return new $name(&$this, $arguments);
		}
		else
		{
			throw new Exception("The helper/method '{$name}' does not exist");
		}
	}
	
	/**
	 * @param	string $name
	 * @return	mixed
	 */
	public function __get($name)
	{
		return isset($this->_vars[$name]) ? $this->_vars[$name] : null;
	}
	
	/**
	 * @param	string $name
	 * @return	bool
	 */
	public function __isset($name)
	{
		return isset($this->_vars[$name]);
	}
	
	public function docHead()
	{
		return	'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'
				. "\n" . '<html xmlns="http://www.w3.org/1999/xhtml" lang="en">';
	}
	
	public function getControllerObj()
	{
		return $this->controllerObj;
	}
}
