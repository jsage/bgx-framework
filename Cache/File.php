<?php 

/**
 * @author	Jens Sage <j.sage@bgx-mail.de>
 */
class Bgx_Cache_File extends Bgx_Cache_Abstract
{
	/**
	 * @param	string $id
	 * @return	string
	 */
	private function getFileFromId(&$id)
	{
		return constant('TMP') . '/' . Bgx_FileUtil::handyFileName(Bgx_Core::index('sitename') . '_' . $id) . '.cache.php';
	}

	public function isCached($id)
	{
		return 
			file_exists($this->getFileFromId($id)) 
			&& 
			(filemtime($this->getFileFromId($id)) >= 
				(time() - (int) Bgx_Core::index('cache_lifetime'))
			);
	}

	public function load($id)
	{
		return $this->isCached($id) ? unserialize(stripslashes(include($this->getFileFromId($id)))) : null;
	}

	public function save($value, $id)
	{
		if (file_exists($this->getFileFromId($id)) && !is_writable($this->getFileFromId($id)))
		{
			throw new Exception('Cache not writeable', 1100);
		}
		return file_put_contents($this->getFileFromId($id), '<?php return \'' . addslashes(serialize($value)) . '\';');
	}

	/**
	 * returns the age of a file
	 *
	 * @param	string $id
	 * @return	int
	 */
	public function cacheAge($id)
	{
		return filemtime($this->getFileFromId($id));
	}

	public function clear($id)
	{
		if (!file_exists($this->getFileFromId($id)))
		{
			return false;
		}
		return unlink($this->getFileFromId($id));
	}

	public function clearAll()
	{
		if (!$handle = opendir(constant('TMP')))
		{
			return false;
		}
		while (false !== ($file = readdir($handle))) {
			if (strpos($file, '.cache.php'))
			{
				@unlink(constant('TMP') . '/'. $file);
			}
		}
		closedir($handle);
		return true;
	}
}