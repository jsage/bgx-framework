<?php
/**
 * @author Jens Sage <j.sage@bgx-mail.de>
 */
class Bgx_Cache_Memcache extends Bgx_Cache_Abstract
{
	/**
	 * @var Memcache
	 */
	private $_memcacheObj;
	
	public function __construct()
	{
		$this->_memcacheObj = new Memcache();
		
		$args = Bgx_Core::index('memcache');
		if (!is_array($args))
		{
			throw new Exception('Memcache-settings have to be an array');
		}
		$this->_memcacheObj->connect(
			$args['host'],
			isset($args['port']) ? $args['port'] : null, 
			isset($args['timeout']) ? $args['timeout'] : null
		);
	}

	public function isCached($id)
	{
		return $this->_memcacheObj->get($id) !== false;
	}

	public function load($id)
	{
		return $this->_memcacheObj->get($id);
	}

	public function save($value, $id)
	{
		return $this->_memcacheObj->set(
			$id, 
			$value, 
			null, 
			(int) Bgx_Core::index('cache_lifetime')
		);
	}

	public function clear($id)
	{
		return $this->_memcacheObj->delete($id);
	}

	public function clearAll()
	{
		return $this->_memcacheObj->flush();
	}
}
	