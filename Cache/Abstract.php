<?php
/**
 * @author Jens Sage <j.sage@bgx-mail.de>
 */
abstract class Bgx_Cache_Abstract
{
	private static $_instance;
	
	/**
	 * is something cached?
	 *
	 * @param	string $id
	 * @return	bool
	 */
	public abstract function isCached($id);
	
	/**
	 * returns a cache with identifier $id
	 *
	 * @param	string $id
	 * @return	mixed
	 */
	public abstract function load($id);
	
	/**
	 * cache data ($value) with identifier ($id)
	 *
	 * @param	mixed	$value
	 * @param	string	$id
	 * @return	bool
	 */
	public abstract function save($value, $id);
	
	/**
	 * clears a cache
	 *
	 * @param	string $id
	 * @return	bool
	 */
	public abstract function clear($id);
	
	/**
	 * @return	bool
	 */
	public abstract function clearAll();
	
	/**
	 * @return Bgx_Cache_Abstract
	 */
	public static function getInstance()
	{
		if (self::$_instance === null)
		{
			if (function_exists('apc_fetch'))
			{
				self::$_instance = new Bgx_Cache_Apc;
			}
			else if (class_exists('Memcache', false) && Bgx_Core::index('memcache'))
			{
				self::$_instance = new Bgx_Cache_Memcache;
			}
			else
			{
				self::$_instance = new Bgx_Cache_File;
			}
		}
		return self::$_instance;
	}
}