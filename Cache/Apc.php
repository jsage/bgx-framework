<?php
/**
 * @author Jens Sage <j.sage@bgx-mail.de>
 */
class Bgx_Cache_Apc extends Bgx_Cache_Abstract
{
	public function isCached($id)
	{
		return apc_fetch($id) !== false;
	}

	public function load($id)
	{
		return apc_fetch($id);
	}

	public function save($value, $id)
	{
		return apc_store($id, $value, (int) Bgx_Core::index('cache_lifetime'));
	}

	public function clear($id)
	{
		return apc_delete($id);
	}

	public function clearAll()
	{
		return apc_clear_cache();
	}
}
	