<?php
/**
 *
 *
 */
class Bgx_Mail_Template
{
	/**
	 * @param	string		$template
	 * @param	array		$vars
	 * @param	int			$language_id
	 * @return	Zend_Mail
	 */
	public static function newMail($template, array $vars = array(), $language_id)
	{
		$sql_n = Bgx_Core::getDb()->quote($template, PDO::PARAM_STR);
		$sql_li = Bgx_Core::getDb()->quote($language_id, PDO::PARAM_INT);
		$template = Bgx_Core::getDb()->query(
			"SELECT t.title AS title, t.message AS message ".
			"FROM mailtemplates AS m " .
			"INNER JOIN mailtemplates_translated AS t ON t.template_id = m.id " . 
			"WHERE m.name = {$sql_n} AND t.language_id = " . $sql_li
		)->fetch(PDO::FETCH_ASSOC);

		if (count($vars))
		{
			foreach ($vars AS $key => $value)
			{
				$template['message'] = str_replace('{' . $key . '}', $value, $template['message']);
			}
		}

		$mail = new Zend_Mail();
		$mail->setBodyText($template['message']);
		$mail->setSubject($template['title']);
		return $mail;
	}

	public static function addTemplate($name)
	{

	}

	public static function deleteTemplate($id)
	{
		$id = intval($id);
		Bgx_Core::getDb()->query("DELETE FROM mailtemplates_translated WHERE template_id = " . $id);
		Bgx_Core::getDb()->query("DELETE FROM mailtemplates WHERE id = " . $id);
	}

	public static function addTranslation($template_id, $title, $message)
	{

	}

	public static function editTranslation($translation_id, $title, $message)
	{

	}

	public static function deleteTranslation($translation_id)
	{

	}
}