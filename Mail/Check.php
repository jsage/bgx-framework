<?php
/**
 * CheckMail
 * @author    Jens Sage <j.sage@babbls.de>
 * @author    Nicolai Buchwitz <n.buchwitz@babbls.de>
 * @package   BgxCms
 */
class Bgx_Mail_Check
{
	private static $blacklist = null;
	const  OK         = 1;
	const  BAD        = 2;
	const  UGLY       = 3;

	public static function check($adress)
	{
		$host = substr($adress, strpos($adress, '@') + 1);

		if (self::onBlacklist($host))
		{
			return self::UGLY;
		}
		else if (!self::onBlacklist($host) && !self::checkMX($host))
		{
			return self::BAD;
		}
		else
		{
			return self::OK;
		}
	}

	/**
	 *
	 * @param	$mail
	 * @return	string
	 */
	public static function checkSyntax($mail)
	{
		return false; // @todo
	}

	/**
	 *
	 * @param	string	$host
	 * @return	boolean
	 */
	private static function onBlacklist($host)
	{
		if (self::$blacklist === null)
		{
			$result = Bgx_Core::getDb()->query("SELECT email FROM email_blacklist");
			self::$blacklist = array();
			while($row = $result->fetch())
			{
				self::$blacklist[] = $row['email'];
			}
		}
		return in_array($host, self::$blacklist);
	}

	/**
	 * @param	string		$host
	 * @retur	boolean
	 */
	private static function checkMX($host) {
		getmxrr($host, $mx_records);

		return count($mx_records) >= 1;
	}
}
