<?php
/**
 * @author Jens Sage
 */
class Bgx_Captcha
{
	private $_possible = 'abcdefghikmnpqrstuvwxyz23456789';
	private $_length = 5;
	private static $instance;

	private function __construct()
	{
		if (!isSet($_SESSION['captcha']['code']))
		{
			$this->_generateCode();
		}
	}

	/**
	 * @return Bgx_Captcha
	 */
	public static function getObject()
	{
	    if (self::$instance === null)
	    {
	        self::$instance = new self;
	    }
	    return self::$instance;
	}

	private function _generateCode()
	{
		$_SESSION['captcha']['code'] = "";
		for ($i = 0; $i < $this->_length; ++$i)
		{
			$_SESSION['captcha']['code'] .= substr($this->_possible, (rand() % (strlen($this->_possible))), 1);
		}
		$_SESSION['captcha']['code'] = strtolower($_SESSION['captcha']['code']);
	}

	/**
	 * @return string
	 */
	public function getCode()
	{
		return (string) $_SESSION['captcha']['code'];
	}

	/**
	 * @return void
	 */
	public function printImg()
	{
		if (headers_sent())
		{
			Bgx_Core::log('captcha eventually wont be sent properly, headers already sent', Zend_Log::CRIT);
		}
		header('Content-type: image/png');
		header("Cache-Control: no-cache, must-revalidate");

		$background = constant('LIBRARY') . '/Bgx/Captcha/background.png';
		if (!file_exists($background))
		{
		    Bgx_Core::log('captcha background image is not available', Zend_Log::CRIT);
			exit;
		}

		$height = 80;
		$width = 150;
		if (!function_exists('imagecreatetruecolor'))
		{
			Bgx_Core::log('GD library is needed for captchas', Zend_Log::CRIT);
			exit;
		}
		$im  = imagecreatetruecolor($width, $height);
		//imageantialias($im, true);
		$font_c = array(imagecolorallocate($im, 70, 70, 72),
		                imagecolorallocate($im, 76, 91, 107),
		                imagecolorallocate($im, 83, 101, 117),
		                imagecolorallocate($im, 255, 189, 0),
		                imagecolorallocate($im, 249, 163, 0));
        imagefill($im, 0, 0, imagecolorallocate($im, 255, 255, 255));

        $letter_width = 20;
        $x_offset = 15;
        $y_offset = $height / 2;
        $y_diff_range = 10;

        imagefttext($im,
    		        27,
    		        0,
    		        $x_offset = 15,
    		        45,
    		        imagecolorallocate($im, 185, 185, 185),
    		        constant('LIBRARY') . '/Bgx/Captcha/Folks.ttf',
    		        Bgx_Core::index('sitename'));

        for ($i = 0; $i < $this->_length; ++$i)
        {
    		imagefttext($im,
    		            18,
    		            0,
    		            $letter_width*$i + $x_offset,
    		            $y_offset + (rand(-($y_diff_range), $y_diff_range)),
    		            $font_c[rand(0, count($font_c)-1)],
    		            constant('LIBRARY') . '/Bgx/Captcha/Folks.ttf',
    		            substr($this->getCode(), $i, 1));
        }
		// OUTPUT
	    imagepng($im);
        imagedestroy($im);
	}

	/**
	 * @param string $input user input
	 */
	public function check($input)
	{
		if (strtolower($input) == $_SESSION['captcha']['code'])
		{
			$return = true;
		}
		else
		{
			$return = false;
		}
		$this->_generateCode();
		return $return;
	}
}