<?php
/**
 * @author Jens Sage <j.sage@bgx-mail.de>
 *
 */
final class Bgx_Event
{
	private static $_observers = array();
	
	/**
	 * Fire an event
	 * 
	 * @param	string	$event_name
	 * @param	mixed	$args
	 */
	public final static function fire($event_name, $args = null)
	{
		foreach (self::$_observers AS $observer_event_name => $observer)
		{
			/**
			 * this observer is affected
			 */
			if ($event_name == $observer_event_name)
			{
				if (is_string($observer))
				{
					$tmp = new $observer();
					$tmp->fire($args);
					
					self::$_observers[$observer_event_name] = $tmp;
				}
				else
				{
					$observer->fire($args);
				}
			}
		}
	}
	
	/**
	 * @param	string			$event_name
	 * @param	Bgx_Observer	$observerObj
	 * @throws	Exception
	 * @return	void
	 */
	public final static function registerObserverObject($event_name, Bgx_Event_Observer $observerObj)
	{
		if (!is_string($event_name))
		{
			throw new Exception('Event-name must be a string');
		}
		
		self::$_observers[$event_name] = $observerObj;
	}
	
	/**
	 * @param	string		$event_name
	 * @param	string		$observer_name
	 * @throws	Exception
	 * @return	void
	 */
	public final static function registerObserver($event_name, $observer_name)
	{
		if (!is_string($event_name) || !is_string($observer_name))
		{
			throw new Exception('Event-name and observer-name must be a string');
		}
		
		if (!class_exists($observer_name))
		{
			throw new Exception('Observer class does not exist');
		}
		
		self::$_observers[$event_name] = $observer_name;
	}
}