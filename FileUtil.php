<?php
/**
 * @file /library/Bgx/FileUtil.php
 */

/**
 * FileUtil
 * @author Jens Sage <j.sage@babbls.de>
 */
class Bgx_FileUtil
{
	protected static $fileHandlerObj = null;

	/**
	 * @param  string $category
	 * @param  string $filename
	 * @return string
	 */
	public static function getUploadedFilePath($category, $filename)
	{
		return constant('APPDIR') . '/uploads/' . $category . '/' . $filename;
	}

	/**
	 * @param  string $cat
	 * @param  string $filename
	 * @return bool
	 */
	public static function deleteUploadedFile($category, $filename)
	{
		return unlink(constant('APPDIR') . '/uploads/' . $category . '/' . $filename);
	}

	/**
	 * @param  string $uploadname
	 * @return string
	 */
	public static function getUploadFileName($uploadname, $index = null)
	{
		return is_null($index) ? $_FILES[$uploadname]['name'] : $_FILES[$uploadname]['name'][$index];
	}

	/**
	 * @param  string $uploadname
	 * @return string
	 */
	public static function getUploadFileType($uploadname, $index = null)
	{
		return is_null($index) ? $_FILES[$uploadname]['type'] : $_FILES[$uploadname]['type'][$index];
	}

	/**
	 * @param  string $uploadname
	 * @return string
	 */
	public static function getTmpUploadFile($uploadname, $index = null)
	{
		return is_null($index) ? $_FILES[$uploadname]['tmp_name'] : $_FILES[$uploadname]['tmp_name'][$index];
	}

	/**
	 * @param  string $uploadname
	 * @return bool
	 */
	public static function fileUploaded($uploadname)
	{
		return isSet($_FILES[$uploadname]);
	}

	/**
	 * moves a uploaded file to target
	 *
	 * return "true" on success
	 * error-constants:
	 * int = 1: UPLOAD_ERR_INI_SIZE (too big by server-size)
	 * int = 2: UPLOAD_ERR_FORM_SIZE (too big by html-form-size)
	 * int = 3: UPLOAD_ERR_PARTIAL (parts of file missing)
	 * int = 4: UPLOAD_ERR_NO_FILE (file is missing)
	 *
	 * @param  string $uploadname fileinput-name
	 * @param  string $category
	 * @param  string $filename
	 * @return int|bool
	 */
	public static function uploadFile($uploadname, $category, $filename = "")
	{
		if ($_FILES[$uploadname]['error'] == UPLOAD_ERR_OK)
		{
			return self::_copyFile($_FILES[$uploadname]['tmp_name'],
			$filename,
			$category,
			$_FILES[$uploadname]['type']);
		}
		else
		{
			return $_FILES[$uploadname]['error'];
		}
	}

	/**
	 * multiple upload
	 *
	 * @see    FileUtil::moveUploadedFile()
	 * @param  string $uploadname
	 * @param  string $target
	 * @return array returns saved files
	 */
	public static function uploadFiles($uploadname, $category)
	{
		$saved_files = array();
		foreach ($_FILES[$uploadname]['error'] as $key => $error) {
			if ($error == UPLOAD_ERR_OK) {

				if (self::_copyFile($_FILES[$uploadname]['tmp_name'][$key],
				$_FILES[$uploadname]['name'][$key],
				$category,
				$_FILES[$uploadname]['type'][$key]))
				{
					$saved_files[] = $_FILES[$uploadname]['name'][$key];
				}
			}
		}

		return $saved_files;
	}

	/**
	 * @param  string $tmpfile
	 * @param  string $save_as
	 * @param  string $category
	 * @param  string $type
	 * @return bool
	 */
	private static function _copyFile($tmpfile, $save_as, $category, $type = "")
	{
		if (!empty($type))
		{
			$blacklist = array('text/php'); // FIXME use a config
			if (in_array($type, $blacklist))
			{
				return false;
			}
		}

		if (!file_exists(self::getUploadedFilePath($category, "")))
		{
			mkdir(self::getUploadedFilePath($category, ''));
		}

		if (!is_writeable(self::getUploadedFilePath($category, '')))
		{
			throw new Exception('Destination ist not writeable.');
		}
		return move_uploaded_file($tmpfile, self::getUploadedFilePath($category, $save_as));
	}

	/**
	 * @param  string $filename
	 * @return string
	 */
	public static function handyFileName($filename)
	{
		return preg_replace('~^[^a-z0-9_.-]$~', '_', $filename);
	}

	/**
	 * Enter description here...
	 *
	 * @param  string $file path to file
	 * @return int
	 */
	public static function getFileAge($file)
	{
		return filemtime($file);
	}

	/**
	 * @param	string $file	path to file
	 * @return	string 			mime-type
	 */
	public static function getFileType($file)
	{
		if (function_exists('mime_content_type'))
		{
			return mime_content_type($file);
		}
		else if (function_exists('finfo_open'))
		{
			finfo_file(finfo_open(FILEINFO_MIME), $file);
		}
		else
		{
			$m = pathinfo($file);
			switch(strtolower($m['extension']))
			{
				case 'js':
					return 'application/javascript';
				case 'json':
					return 'application/json';
				case 'jpg':
				case 'jpeg':
				case 'jpe':
					return 'image/jpg';
				case 'png':
				case 'gif':
				case 'bmp':
					return 'image/' . strtolower(isSet($m[1]) ? $m[1] : 'bmp');
				case 'css':
					return 'text/css';
				case 'xml':
					return 'application/xml';
				case 'html':
				case 'htm':
					return 'text/html';
				default:
					if ($m = trim(exec('file -b --mime '.escapeshellarg($file))))
					{
						return $m;
					}
					return 'text/plain';
			}
		}
	}

	public static function getPictureDimensions($file)
	{
		if (!file_exists($file))
		{
			return false;
		}
		$info = getimagesize($file);
		return array('width' => &$info[0], 'height' => &$info[1]);
	}
}