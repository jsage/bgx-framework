<?php

class Bgx_View_Helper_Scrolling extends Bgx_View_Helper_Abstract
{
	
	public function __toString()
	{
		$page = $this->_params[0];
		$pages = $this->_params[1];
		return '<div class="pagination"> Seite 1 von ' . $pages . '</div>';
	}
}