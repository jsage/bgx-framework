<?php

abstract class Bgx_View_Helper_Abstract
{
	/**
	 * @var Bgx_View
	 */
	protected $_view;
	
	protected $_params = array();
	
	public function __construct(Bgx_View $view, $params = array())
	{
		$this->_view = $view;
		$this->_params = $params;
	}
	
	public abstract function __toString();
}