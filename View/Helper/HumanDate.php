<?php
/**
 * @author Jens Sage <j.sage@bgx-mail.de>
 *
 */
class Bgx_View_Helper_HumanDate extends Bgx_View_Helper_Abstract
{
	
	public function __toString()
	{
		if ($time = strtotime($this->_params[0]))
		{
			return (string) Bgx_Gimmicks::humanDate(new Zend_Date($time));
		}
		else
		{
			return "";
		}
	}
}