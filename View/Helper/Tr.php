<?php
/**
 * @author Jens Sage <j.sage@bgx-mail.de>
 */
class Bgx_View_Helper_Tr extends Bgx_View_Helper_Abstract
{
	
	public function __toString()
	{
		return Bgx_Core::translate()->translate($this->_params[0]);
	}
}