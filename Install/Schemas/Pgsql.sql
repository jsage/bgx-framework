CREATE TABLE languages (
	"id" serial PRIMARY KEY UNIQUE,
	"identifier" varchar(5) NOT NULL UNIQUE,
	"active" boolean
);

CREATE TABLE users (
	"id" serial PRIMARY KEY,
	"username" varchar(16) NOT NULL UNIQUE,
	"password" varchar(32) NOT NULL,
	"email" varchar(100) NOT NULL UNIQUE,
	"active" boolean NOT NULL DEFAULT(false),
	"lastactivity" timestamp,
	"reg_date" date DEFAULT NOW(),
	"language" smallint,
	FOREIGN KEY ("language") REFERENCES languages("id")
);

CREATE TABLE groups (
	"id" serial PRIMARY KEY UNIQUE,
	"name" varchar(50) UNIQUE,
	"extends" integer,
	"superuser" bool NOT NULL DEFAULT (false),
	"description" varchar(200),
	FOREIGN KEY ("extends") REFERENCES groups("id")
);

CREATE TABLE permissions (
	"id" serial PRIMARY KEY,
	"name" varchar(50) UNIQUE
);

CREATE TABLE users_to_groups (
	"bind_id" serial PRIMARY KEY,
	"user_id" integer NOT NULL,
	"group_id" integer NOT NULL,
	FOREIGN KEY ("user_id") REFERENCES users ("id") ON UPDATE RESTRICT,
	FOREIGN KEY ("group_id") REFERENCES groups ("id") ON UPDATE RESTRICT
);

CREATE TABLE group_rights (
	"id" serial PRIMARY KEY,
	"group_id" integer NOT NULL,
	"perm_id" integer NOT NULL,
	"allow" bool NOT NULL DEFAULT(false),
	FOREIGN KEY ("group_id") REFERENCES groups("id"),
	FOREIGN KEY ("perm_id") REFERENCES permissions("id")
);

CREATE TABLE language_vars (
	"key_id" serial PRIMARY KEY,
	"key" varchar(250) NOT NULL
);

CREATE TABLE language_vars_translated (
	"id" serial PRIMARY KEY,
	"key_id" integer NOT NULL UNIQUE,
	"lang_id" integer NOT NULL,
	"translation" text NOT NULL,
	FOREIGN KEY ("lang_id") REFERENCES languages ("id"),
	FOREIGN KEY ("key_id") REFERENCES language_vars ("key_id")
);

CREATE TABLE sites (
	"id" serial PRIMARY KEY,
	"name" varchar unique NOT NULL,
	"embedded" bool NOT NULL DEFAUlT(false),
	"deletable" bool NOT NULL DEFAULT(true)
);

CREATE TABLE sitecontent (
	"id" serial PRIMARY KEY,
	"site_id" integer NOT NULL,
	"title" varchar,
	"language_id" integer NOT NULL,
	"content"text,
	FOREIGN KEY ("site_id") REFERENCES sites ("id"),
	FOREIGN KEY ("language_id") REFERENCES languages ("id")
);

CREATE TABLE email_blacklist (
	"id" serial PRIMARY KEY,
	"email" varchar NOT NULL UNIQUE
);

CREATE TABLE logs (
	"id" serial PRIMARY KEY,
	"level" smallint,
	"message" text,
	"time" timestamp DEFAULT NOW()
);

CREATE TABLE settings (
	"id" serial PRIMARY KEY,
	"var" varchar NOT NULL UNIQUE,
	"value" text,
	"type" varchar DEFAULT('string')
);

CREATE TABLE register_codes (
	"id" serial PRIMARY KEY,
	"user_id" int NOT NULL UNIQUE,
	"code" varchar NOT NULL UNIQUE,
	FOREIGN KEY ("user_id") REFERENCES "users" ("id")
);

CREATE TABLE mailtemplates (
	"id" serial PRIMARY KEY,
	"name" varchar UNIQUE
);

CREATE TABLE mailtemplates_translated (
	"id" serial PRIMARY KEY,
	"template_id" int NOT NULL,
	"language_id" int NOT NULL,
	"title" varchar,
	"message" text,
	FOREIGN KEY ("template_id") REFERENCES "mailtemplates" ("id"),
	FOREIGN KEY ("language_id") REFERENCES "languages" ("id")
);