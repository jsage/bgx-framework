<?php
/**
 * @author Jens Sage <j.sage@bgx-mail.de>
 * @package BgxCms
 */
class Bgx_Debug
{
	private static function debugHead()
	{
		echo '<div style="margin:10px;padding:10px;background:' . Bgx_Gimmicks::getColor('aluminium1') .
		     ';border:1px solid ' . Bgx_Gimmicks::getColor('aluminium3') .
		     ';font-family:monospace;text-align:left;">' .
		     '<div style="width:65%;float:left"><h2 style="margin:0;padding:5px;font-size:12px;font-family:sans-serif;color:' . Bgx_Gimmicks::getColor('skyblue3') .
		     '">BgxCms Debug</h2>' .
		     '<div style="margin:5px;padding:10px;border:1px solid ' . Bgx_Gimmicks::getColor('aluminium2') .
		     ';background:white;font-size:10px;">';
	}

	private static function debugFoot()
	{
		echo '</div></div>';
		echo '<div style="width:33%;float:left">' .
		     '<h2 style="margin:0;padding:5px;font-size:12px;font-family:sans-serif;color:' . Bgx_Gimmicks::getColor('skyblue3') .
		     '">Trace</h2><ul style="margin:0;padding:0;margin-left:7px;list-style:none">';

		$trace = debug_backtrace();
		foreach ($trace AS $info)
		{
            echo '<li style="margin-bottom:5px;padding:4px;border:1px solid ' . Bgx_Gimmicks::getColor('aluminium2') .
                 ';background:white;display:block;color:black' . "\">{$info['class']}{$info['type']}{$info['function']}(";
            /**foreach ($info['args'] AS $arg)
            {
            	echo substr(strval($arg),0,20) . ', ';
            }
            echo ") @{$info['file']}:{$info['line']}";*/
            echo ") </li>";
		}
		echo '</ul></div>';
		echo '<div style="height:1px;clear:both"></div>';
		echo '</div>';
	}

	/**
	 * @param	mixed	$variable
	 * @return	void
	 */
	public static function dump($variable = null, $only_in_debug_mode = true)
	{
		if ($variable instanceof Exception)
		{
			$variable = $variable->getMessage();
		}
		if ($only_in_debug_mode && !Bgx_Core::index('debug'))
		{
			return;
		}

		self::debugHead();
		echo '<pre style="color:#000">';
		var_dump($variable);
		echo '</pre>';
		self::debugFoot();
	}

	/**
	 * @return void
	 */
	public static function showRegistry()
	{
		$registry = Bgx_Core::getRegistry();
		self::debugHead();
		echo '<ul>';
		foreach ($registry AS $var => $value)
		{
			echo '<li ><h3>' . $var . ':</h3><pre>';
			ob_start();
			var_dump($value);
			$output = ob_get_contents();
			ob_end_clean();
			echo htmlentities($output) . '</pre></li>';
		}
		echo '</ul>';
		self::debugFoot();
	}

	/**
	 * @param	string	$sql
	 */
	public static function dumpSql($sql)
	{
        $sql = strval($sql);
        //[dv] this has to come first or you will have goofy results later.
        $sql = preg_replace("/['\"]([^'\"]*)['\"]/i", "'<FONT COLOR='#FF6600'>$1</FONT>'", $sql, -1);

        $sql = str_ireplace(array('*',
                                  'SELECT ',
                                  'UPDATE ',
                                  'DELETE ',
                                  'INSERT ',
                                  'INTO ',
                                  ' VALUES ',
                                  ' FROM ',
                                  'LEFT',
						          'INNER',
						          'RIGHT',
                                  'JOIN',
                                  'WHERE',
                                  'LIMIT',
                                  'ORDER BY',
                                  'AND',
                                  'OR ',
                                  ' DESC',
                                  'ASC',
                                  'ON '),
                            array('<span style="color:' . Bgx_Gimmicks::getColor('plum2') . '">*</span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue3') . '">SELECT </span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue3') . '">UPDATE </span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue3') . '">DELETE </span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue3') . '">INSERT </span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue2') . '">INTO </span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue2') . '"> VALUES </span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue2') . '"> FROM </span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('chocolate2') . '">LEFT</span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('chocolate2') . '">INNER</span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('chocolate2') . '">RIGHT</span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('chocolate2') . '">JOIN</span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue2') . '">WHERE</span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue2') . '">LIMIT</span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue2') . '">ORDER BY</span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('scarletred2') . '">AND</span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('scarletred2') . '">OR </span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue2') . '"> DESC</span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('skyblue2') . '">ASC</span>',
                                  '<span style="color:' . Bgx_Gimmicks::getColor('chocolate2') . '">ON </span>',
                                  ),
                            $sql
                          );
        self::debugHead();
        echo '<span style="color:' . Bgx_Gimmicks::getColor('aluminium6') . '">' . $sql . '</span>';
        self::debugFoot();
	}
}