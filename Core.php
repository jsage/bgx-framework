<?php
session_start();
set_magic_quotes_runtime(0);

define('LIBRARY', realpath(dirname(__FILE__) . '/../'));

/**
 * classnames contain route to files ("_" = "/")
 * @param string $class
 */
function __autoload($class)
{
	if (!include_once str_replace('_', DIRECTORY_SEPARATOR, $class . '.php'))
	{
		$i = max(debug_backtrace());
		
		Bgx_Core::log("{$class} not found, requested by " . $i['file'] . ':' . $i['line']);
		exit;
	}
}


/**
 * this is missing in php!
 *
 * @param  array $array
 * @param  int|string $index
 * @return mixed
 */
function array_value(array $array, $index)
{
	return isSet($array[$index]) ? $array[$index] : null;
}

/**
 * We carry the can for the php guys disability
 */
if (!function_exists('lcfirst'))
{ 
    function lcfirst($str)
    {
    	return (string) strtolower(substr($str, 0, 1)) . substr($str, 1);
    } 
}

/**
 * translate
 * @param	string	$string
 * @return	string
 */
function tr($string, $html_entities = false)
{
	$o = Bgx_Core::translate()->translate($string);
	if ($html_entities)
		return htmlentities($o);
	return $o;
}

/**
 * @return	Bgx_User_Abstract
 */
function user()
{
	return Bgx_User_Auth::getUserObj();
}

/**
 * @param	string	$action
 * @return	bool
 */
function can($action)
{
	return Bgx_User_Auth::getUserObj()->isAuthorisedTo($action);
}

/**
 * 
 * @param	string		$url
 * @param	string		$message
 * @return	void
 */
function redirect($url = null, $message = null)
{
	$message = is_null($message) ? tr("You will be redirected") : $message;
	$url = ($url===null) ? (
			isset($_SERVER['HTTP_REFERER']) ? 
				$_SERVER['HTTP_REFERER'] : 
				Bgx_Request::getInstance()->getBaseUrlString() ) : 
			$url;

	if ($url == Bgx_Request::getInstance()->getUrlString())
	{
		$url = Bgx_Mvc::getBaseUrlString();
	}
	try
	{
		if (headers_sent())
		{
			$message = htmlentities($message);
			echo '<?xml version="1.0" encoding="utf-8"?>' .
			'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" ' .
			'"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' .
			'<html xmlns="http://www.w3.org/1999/xhtml">' .
			'<head><meta http-equiv="refresh" content="0; URL=http://de.selfhtml.org/">' .
			"</head><body><h2>{$message}</h2></body></html>";
		}
		else
		{
			header('Location: ' . $url);
		}
	}
	catch (Exception $e)
	{
		Bgx_Core::log('Bgx_HeaderHandler wasnt able to redirect to "' . $url . '"');
	}
	exit;
}

class SqlError extends Exception {
	function __construct()
	{
		parent::__construct("", 0);
		
		$this->code = Bgx_Core::getDb()->errorCode();
		$this->message = 'SQL Error: ' . array_value(Bgx_Core::getDb()->errorInfo(), 2);
	}
}

/**
 * @author	Jens Sage <j.sage@babbls.de>
 * @package	BgxCms
 */
final class Bgx_Core
{
	/**
	 * @var object
	 */
	private static $instance = null;

	/**
	 * @var array
	 */
	private static $registry = array();

	/**
	 * @return void
	 */
	private function __construct()
	{
		error_reporting(self::index('debug') ? E_ALL : 0);
		ini_set('display_errors', self::index('debug') ? 'On' : 'Off');
		
		Bgx_Controller_Router::addControllerDir(self::index('controller_dir'));
		Bgx_Controller_Router::addControllerDir(LIBRARY . '/Bgx/ControlInterface/Controller/');

		date_default_timezone_set('Europe/Berlin');
		//setlocale(); @todo 
		// set encoding @todo
		
		Bgx_Event::fire(__METHOD__);
		
		Bgx_Controller_Router::executeUserRequest();
	}

	/**
	 * @return void
	 */
	public function __destruct()
	{
		Bgx_Event::fire(__METHOD__);
	}

	private function __clone() {}

	/**
	 * @return void
	 */
	public static final function run(array $registry)
	{
		if (count($registry))
		{
			foreach ($registry AS $key => $value)
			{
				self::register($key, $value);
			}
		}

		self::$instance = new self;
	}

	/**
	 * @param  string $id
	 * @param  mixed $value
	 */
	public static final function register($id, $value)
	{
		self::$registry[(string) $id] = $value;
	}

	/**
	 * @param  string $id
	 * @return mixed|null
	 */
	public static final function index($id)
	{
		return isSet(self::$registry[$id]) ? self::$registry[$id] : NULL;
	}

	/**
	 * @return array
	 */
	public static final function getRegistry()
	{
		return self::$registry;
	}

	/**
	 * @param string $id
	 * @param string $append
	 */
	public static final function appendIndex($id, $append)
	{
		if (!isSet(self::$registry[$id]))
		{
			self::$registry[$id] = "";
		}

		if (!is_array(self::$registry[$id]) || !is_object(self::$registry[$id]))
		{
			self::$registry[$id] .= $append;
		}
	}
	
	/**
	 * @return PDO
	 * @throws Exception
	 */
	public static final function getDb()
	{
		if (self::index('database_obj') === null)
		{
			try
			{
			    self::register(
			    	'database_obj', 
			    	new PDO(
			    		self::index('db_type') . 
			    		':dbname=' . self::index('db_name') . 
			    		';host=' . self::index('db_host'), 
			    		self::index('db_username'), 
			    		self::index('db_password')
			    	)
			   	);
			} catch (PDOException $e)
			{
				throw new Exception($e->getMessage(), $e->getCode());
			}
		}
		return self::index('database_obj');
	}

	/**
	 * @return Bgx_Language
	 */
	public static final function translate()
	{
		if (is_null(self::index('language_obj')))
		{
			self::register('language_obj', new Bgx_Language());
			self::index('language_obj')->detectBrowserLanguage();
		}
		return self::index('language_obj');
	}

	/**
	 * @param  string $message
	 * @param  int $code
	 * @return void
	 */
	public static final function log($message, $code = '0000')
	{
		$i = max(debug_backtrace());
		
		error_log(
			date('r') . " " . $i['file'] . " " . 
			$i['function'] . "(): #{$code} {$message}", 
			0
		);
		
		if (self::index('debug'))
			trigger_error($message, E_USER_NOTICE);
	}


	/**
	 * @return Bgx_Cache_Abstract
	 */
	public static final function getCache()
	{
		return Bgx_Cache_Abstract::getInstance();
	}

	/**
	 * @return Bgx_Request
	 */
	public static function getRequest()
	{
		return Bgx_Request::getInstance();
	}
	
	/**
	 * @return Bgx_Controller_Router
	 */
	public static function getRouter()
	{
		return Bgx_Controller_Router::getInstance();
	}
}
