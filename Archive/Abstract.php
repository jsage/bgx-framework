<?php
/**
 * @author  Jens Sage <j.sage@bgx-mail.de>
 * @package BgxCms
 */
abstract class Bgx_Archive_Abstract
{
	/**
	 * There is no need, that the archive exists.
	 *
	 * @param  string $archive path + basename + suffix of the (possible) archive
	 */
	public abstract function __construct($archive);

	/**
	 * @param  unknown_type $target
	 * @return bool
	 */
	public abstract function extract($target);

    /**
     * @return array
     */
	public abstract function getContent();

    /**
     * @param  string $dir     dir-name
     * @param  string $target  where to store it (in the archive)
     * @return Bgx_Archive_Abstract
     */
	public abstract function addDir($dir, $target = './');

	/**
	 * @param  string $file    path to existing file
	 * @param  string $target  where to store it (in the archive)
	 * @return Bgx_Archive_Abstract
	 */
	public abstract function addFile($file, $target = './');
}