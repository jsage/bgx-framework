<?php

abstract class Bgx_Search_Engine_Abstract implements ArrayAccess
{
	/**
	 * @var array
	 */
	protected $result = array();

	/**
	 * @param  string $searchterm
	 * @return void
	 */
	public abstract function search($searchterm);

	/**
	 * @param array $options
	 */
	public abstract function setOptions(array $options);

	/**
	 * @param  string $searchterm
	 * @return array
	 */
	protected function searchTermParts($searchterm)
	{

	}

	/**
	 * @return bool
	 */
	public function offsetExists($offset)
	{
		return array_key_exists($this->result, $offset);
	}

	/**
	 * @return mixed
	 */
	public function offsetGet($offset)
	{
		return $this->result[$offset];
	}

	/**
	 * @return void
	 */
	public function offsetSet($offset, $value)
	{
		if (is_null($offset))
		{
			$this->result[] = $value;
		}
		else
		{
			$this->result[$offset] = $value;
		}
	}

	/**
	 * @return void
	 */
	public function offsetUnset($offset)
	{
		unset($this->result[$offset]);
	}
}