<?php
/**
 * @author	Jens Sage <j.sage@bgx-mail.de>
 */
abstract class Bgx_Controller_Abstract
{
	/**
	 * @var Bgx_View
	 */
	private $_view = null;
	
	/**
	 * @param	string	$str
	 * @return	string
	 */
	protected function _getSubsite(&$str)
	{
		$pos = strpos($str, 'AjaxAction');
		if ($pos == false)
		{
			$pos = strpos($str, 'Action');
		}
		return substr($str, 0, $pos);
	}

	/**
	 * @return	Bgx_View
	 */
	protected function _view()
	{
		if ($this->_view == null)
		{
			$this->_view = new Bgx_View($this);
		}
		return $this->_view;
	}
	
	/**
	 * @param	string|int	$index
	 * @return	string
	 */
	protected function param($index)
	{
		return is_int($index) ?
			Bgx_Request::getInstance()->getVarAtIndex($index) :
			Bgx_Request::getInstance()->getValue($index);
	}
	
	public function __call($name, $args)
	{
		$action = substr($name, 0, strpos($name, 'Action'));
		foreach (Bgx_Controller_Router::getControllerDirs() AS $dir)
		{
			$controller = Bgx_Request::getInstance()->getVarAtIndex(0);
			$path = $dir . "{$controller}.{$action}.php";

			if (!file_exists($path))
			{
				continue;
			}
			else
			{
				include_once($path);
			}
			return false;
		}
	}
}
