<?php
/**
 * @author Jens Sage <j.sage@bgx-mail.de>
 *
 */
class Bgx_Controller_Router
{
	private static $_routes = array(
		0 => array(),
		1 => array()
	);
	
	private static $dirs = array();
	
	public static function addControllerDir($dir)
	{
		if (file_exists($dir))
		{
			self::$dirs[] = $dir;
		}
	}
	
	public static function removeControllerDir($dir)
	{
		if (in_array($dir))
		{
			unset(self::$dirs[array_search($dir, self::$dirs, true)]);
		}
	}
	
	public static function getControllerDirs()
	{
		return self::$dirs;
	}
	
	/**
	 * @param	string		$request		
	 * @param	string		$controller
	 * @param	string		$action			
	 * @return	void
	 */
	public static function defineRoute($request, $controller, $action = null)
	{
		if ($params = explode('/', $request))
		{
			self::$_routes[0][$params[0]] = $controller;
			self::$_routes[1][$params[0]] = $action;
		}
	}
	
	/**
	 * @return	void
	 * @throws	Exception
	 */
	public static function executeUserRequest()
	{
		$controller = Bgx_Request::getInstance()->getVarAtIndex(0);
		$controller = ($controller == null) ? 'Index' : $controller;
		$action = Bgx_Request::getInstance()->getVarAtIndex(1);
		
		if (array_key_exists($controller, self::$_routes[0]))
		{
			$action = self::$_routes[1][$controller];
			$controller = self::$_routes[0][$controller];
		}
		
		if ($action == null)
		{
			$action = lcfirst($controller);
		}
		$action = $action . 'Action';
		
		foreach (self::$dirs AS $dir)
		{
			$path = $dir . $controller . '.php';
			if ((file_exists($path) && include_once($path)) || class_exists($controller . 'Controller'))
			{
				if (($controller = $controller . 'Controller') && class_exists($controller))
				{
					$controllerObj = new $controller();
					
					if ($controllerObj->{$action}() == null)
					{
						return;
					}
				}
				else
				{
					throw new Exception('Controller is not defined in controller-file');
				}
			}
		}
		
		// if not found
		header('HTTP/1.1 404 Not Found');
		if (Bgx_Request::getInstance()->getVarAtIndex(0) == 'SiteNotFound')
		{
			throw new Exception("SiteNotFound Controller missing");
			return;
		}
		Bgx_Request::getInstance()->setVarAtIndex(0, 'SiteNotFound');
		Bgx_Request::getInstance()->setVarAtIndex(1, null);
		self::executeUserRequest();
	}
}
