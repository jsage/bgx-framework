<?php
/**
 * Language
 * @author  Jens Sage <j.sage@bgx-mail.de>
 * @package BgxCms
 */
class Bgx_Language
{
	private $translations = array();
	private $languages = array();

	private $language = 'en';
	
	/**
	 * constructs the language-class
	 * @return	void
	 * @throws	SqlError
	 */
	public function __construct($default = 'en')
	{
		$cache = Bgx_Core::getCache()->load('languages');
		if (!$cache)
		{
			$result = Bgx_Core::getDb()->query("SELECT identifier, id FROM languages WHERE active = true");
			if (!$result)
			{
				throw new SqlError();
				return;
			}
			while ($row = $result->fetch())
			{
				$this->languages[$row['id']] = $row['identifier'];
			}
		}
		else
		{
			$this->languages = $cache;
		}
		
		$this->setLanguage($default);
	}
	
	/**
	 * returns an array with available languages
	 * @return	array
	 */
	public function getLanguages()
	{
		return $this->languages;
	}
	
	public function getLanguage()
	{
		return $this->language;
	}
	
	public function setLanguage($language)
	{
		if (in_array($language, $this->languages, true))
		{
			$this->language = $language;
		}
		return $this;
	}

	public function detectBrowserLanguage()
	{
		$this->setLanguage(substr(getenv('HTTP_ACCEPT_LANGUAGE'),0,2));
		return $this;
	}
	
	public function __destruct()
	{
		if (!Bgx_Core::getCache()->isCached('available_languages'))
		{
			Bgx_Core::getCache()->save($this->languages, 'available_languages');
		}
	}

	/**
	 * @param	string	$language
	 */
	protected function _loadTranslations()
	{
		$this->translations[$this->language] = Bgx_Core::getCache()->load('translations_' . $this->language);
		if (!$this->translations[$this->language])
		{
			$result = Bgx_Core::getDb()->query(
				"SELECT lv.key AS k, lvt.translation AS t FROM language_vars_translated AS lvt " .
				"INNER JOIN language_vars AS lv ON lv.key_id = lvt.key_id ".
				"WHERE lvt.lang_id = " . $this->getNumByIdentifier($this->language)
			);
			while ($item = $result->fetch())
			{
				$this->translations[$this->language][$item['k']] = stripslashes($item['t']);
			}
			Bgx_Core::getCache()->save($this->translations, 'translations_' . $this->language);
		}
	}

	/**
	 * returns the identifier by num-id
	 * @param	int		$num
	 * @return	string|bool
	 */
	public function getIdentifierByNum($num)
	{
		return isSet($this->languages[$num]) ? $this->languages[$num] : false;
	}

	/**
	 * returns the num-id by identifier
	 *
	 * @param	string		$identifier
	 * @return	int|bool
	 */
	public function getNumByIdentifier($identifier)
	{
		if (!is_array($this->languages))
		{
			return false;
		}
		return array_search($identifier, $this->languages, false);
	}

	/**
	 * counts available languages
	 *
	 * @return	int
	 */
	public function languagesAmount()
	{
		return count($this->languages);
	}

	/**
	 * @return int
	 */
	public function getCurrentLanguageId()
	{
		return $this->getNumByIdentifier($this->language);
	}

	/**
	 * @param	string	$string
	 * @return	string
	 */
	public function translate($string, $vars = array())
	{
		if (!isset($this->translations[$this->language]))
		{
			$this->_loadTranslations();
		}
		
		$return = isset($this->translations[$this->language][$string]) ?
			$this->translations[$this->language][$string] : 
			$string;
			
		if (count($vars))
		{
			foreach ($vars AS $var => $value)
			{
				$return = str_replace("{{$var}}", $value, $return);
			}
		}
		
		return $return;
	}
}